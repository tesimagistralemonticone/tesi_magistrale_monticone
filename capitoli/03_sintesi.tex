\chapter{Sintesi delle frasi matematiche} 
\section{Differenza tra linguaggio scritto e quello parlato}
Il linguaggio parlato e quello scritto, anche se strettamente connessi, non sono del tutto equivalenti. Questo è dovuto al fatto che in entrambi i linguaggi sono presenti degli elementi non appartenenti al lessico che aggiungono significato. Ad esempio nello scritto vi sono la punteggiatura, l'organizzazione in paragrafi e l'uso di maiuscole mentre nel parlato sono presenti le pause, la sillabazione e l'intonazione\footnote{anche la gestualità fa parte del linguaggio parlato ma non verrà considerata nell'ambito di questo lavoro.}. 

\subsection{Elementi caratterizzanti del parlato}
Considerando il linguaggio parlato, elementi tipici che lo caratterizzano sono
\begin{itemize}
    \item \textbf{intonazione}: usata per dare enfasi a determinate parti della frase. Ad esempio in genere si tende ad aumentare il tono alla fine di una frase interrogativa e ad abbassarlo alla fine di una dichiarativa. Nello scritto queste informazioni erano invece rappresentate rispettivamente dal punto interrogativo e dal punto. 
    \item \textbf{sillabazione}: usata per denotare che una particolare parola è un acronimo. Nello scritto la stessa funzione poteva essere  rappresentata dal maiuscolo.
    \item \textbf{pause}: usate per separare sintagmi, frasi e paragrafi. Nello scritto la stessa funzione era svolta dalle virgole e dagli ``\textit{a capo}''.      
\end{itemize} 

\subsection{SSML}
Allo scopo di manipolare gli elementi caratteristici del parlato è stato introdotto un linguaggio noto come \texttt{SSML} (\emph{Speech Synthesis Markup Language)}\cite{ssml}. Esso permette di dare delle direttive ai software per la sintesi vocale attraverso dei tag \texttt{XML}. Un piccolo esempio di testo arricchito con \texttt{SSML} è riportato nel listato seguente.
\begin{lstlisting}[style=xml]
<speak>
    <prosody rate="slow">
        a più <break time="500ms"/> tre diviso due.
    </prosody>
</speak>    
\end{lstlisting}          
In questo caso l'attributo \emph{rate} del tag \emph{prosody} indica che la frase deve essere pronunciata lentamente mentre il tag \emph{break} indica che ci deve essere una pausa di 500 ms. Si rimanda alla documentazione ufficiale l'elenco esaustivo dei tag \texttt{SSML}. 

\subsection{Matematica parlata}
Durante la sintesi delle formule matematiche, l'intonazione non gioca un ruolo importante. Si è infatti lasciato il compito di manipolarla al sintetizzatore vocale, il quale si limita al cambio del tono alla fine della frase.

Seppur in matematica gli acronimi vengono comunemente usati (ad esempio \texttt{MCD} per indicare il massimo comune divisore), nelle formule la loro presenza è quasi nulla. Inoltre non sono direttamente rappresentabili dagli operatori disponibili in \LaTeX. Per questi motivi durante la fase di sintesi non è stato necessario sfruttare la sillabazione.

Le pause invece ricoprono un ruolo fondamentale nella sintesi delle formule matematiche e il loro utilizzo sarà discusso in modo approfondito nella sezione \ref{sec:aggregazione_sottoformule}. 

\section{Aggregazione delle sottoformule}\label{sec:aggregazione_sottoformule}

A livello concettuale un'espressione matematica è rappresentabile da una struttura ad albero chiamata \emph{albero delle espressioni}\footnote{questo tipo di albero è molto simile agli alberi di parsing dei linguaggi di programmazione.}. 

\begin{figure}[h]
    \begin{subfigure}[b]{.49\linewidth}
        \centering
        \begin{tikzpicture}[
            level 1/.style={sibling distance=5em},
            % level 2/.style={sibling distance=5em}
            every node/.style={circle,draw,thick, minimum size=1cm}
            ]
            \node {/}
                child {  node {a}}
                child {  node {+}
                    child { node{b}}
                    child { node {c}}           
                };
        \end{tikzpicture}    
        \caption{}
        \label{subfig:exp_tree_a}
    \end{subfigure} %   
    \begin{subfigure}[b]{.49\linewidth}
        \centering
        \begin{tikzpicture}[
            level 1/.style={sibling distance=5em},
            % level 2/.style={sibling distance=5em}
            every node/.style={circle,draw,thick, minimum size=1cm}
            ]
            \node {+}
                child {  node {/}
                    child { node{a}}
                    child { node {b}}           
                }
                child {  node {c}};
        \end{tikzpicture}
        \caption{}
        \label{subfig:exp_tree_b}
    \end{subfigure} 
    \caption{La figura \ref{subfig:exp_tree_a} rappresenta l'espressione $a/(b+c)$ mentre la figura \ref{subfig:exp_tree_b} rappresenta l'espressione $(a/b) + c$.} 
    \label{fig:exp_tree}
\end{figure}
In figura \ref{fig:exp_tree} si possono vedere due esempi di albero delle espressioni per le formule $a/(b+c)$ e $(a/b) + c$. Come si può osservare questa rappresentazione non presenta alcuna ambiguità in quanto la struttura dell'albero definisce intrinsecamente l'ordine con cui devono essere eseguite le operazioni.    
% I due alberi rappresentati in figura \ref{fig:exp_tree} possono essere letti rispettivamente come ``\emph{La divisione tra a e il risultato della somma fra b e c}'' e ``\emph{il risultato della divisione tra a e b sommato con c }''. 
Una tale rappresentazione però non si presta bene quando una formula deve essere comunicata tra esseri umani. Si preferisce infatti una notazione più semplice e compatta. A tale scopo viene eseguita una linearizzazione dell'albero, comprimendolo così in una stringa che può essere usata più agevolmente per trasmettere il significato della formula. Questa operazione di linearizzazione però genera una rappresentazione potenzialmente ambigua. Riconsiderando i due alberi in figura \ref{fig:exp_tree} si nota che entrambe le linearizzazioni dei due alberi portano ad una stessa rappresentazione: 
$$ a / b + c$$ 
È dunque necessario introdurre qualcosa di esterno all'espressione per preservare l'ordine con cui devono essere eseguite le operazioni. La prima cosa che è possibile aggiungere è la parentesizzazione delle sottoformule per specificare come aggregare le sottoformule e quindi preservarne l'ordine di valutazione. Ad esempio la seguente espressione
\begin{center}
    \begin{tikzpicture}[
        level 1/.style={sibling distance=10em},
        level 2/.style={sibling distance=5em},
        every node/.style={circle,draw,thick, minimum size=1cm}
        ]
        \node {+}
            child {  node {a}}
            child {  node {-}
                child { node{/}
                child {  node {c}}
                child {  node {d}}
                }
                child { node {d}}           
            };
    \end{tikzpicture}
\end{center}
può essere linearizzata e parentesizzata come segue:
\begin{equation}\label{eqt:utt_prior}
    (a + ((b / c) - d))
\end{equation}
Una notazione del genere, seppur corretta, è poco leggibile dato il numero elevato di parentesi. Per diminuirne il numero sono state assegnate delle priorità agli operatori in modo da rimuovere determinate parentesi. 
\subsection{Priorità della matematica scritta}
Per convenzione moltiplicazione e divisione hanno un priorità maggiore rispetto alle sottrazioni e divisioni. In questo modo l'espressione \ref{eqt:utt_prior} può essere riscritta come  
\begin{equation*}
    a + b / c - d
\end{equation*}
Questo modalità di parentesizzazione e di assegnamento delle priorità è una convenzione usata nella notazione matematica. Bisogna però considerare che il linguaggio matematico è pensato principalmente per essere scritto. Ci si è quindi chiesti se le stesse convenzioni potessero andare bene anche quando la notazione matematica viene usata nel parlato. 
\subsection{Priorità della matematica parlata}\label{subsec:spoken_priority}
Si è osservato che pronunciando una formula si tende ad assegnare implicitamente delle priorità che non corrispondono con quelle usate nella matematica scritta. Ad esempio nel parlato la formula  
$$\text{\textit{``x più uno diviso x meno due''}}$$
viene in genere percepita come
$$\frac{x+1}{x-2} \qquad \text{cioè} \qquad (x + 1) / (x - 2)$$  
O ancora
$$\text{\textit{``x più uno per x meno due''}}$$
viene in genere percepita come
$$(x + 1) * (x - 2)$$  
Dagli esempi ciò che si evince è che somme e sottrazioni abbiano priorità più alta rispetto alle divisioni e alle moltiplicazioni.  
% TODO: Dire anche che è facile e preciso parentesizzare sempre ma ciò rende troppo pesante il parlato. Spiegare quindi quali sono i tre metodi che permettono di rimuovere più parentesi possibili: principalmente dire che nel parlato a + 1 / b + 1 equivale a (a + 1) / (b + 1) nello scritto. 
\subsection{Il ruolo delle pause nella matematica parlata}
Si è visto come le priorità degli operatori possano aiutare a diminuire il numero di parentesi. Ovviamente queste ultime sono comunque necessarie per alterare il normale ordine di valutazione delle operazioni. 

Nel linguaggio parlato la stessa funzione delle parentesi può essere ottenuta anche con le pause. Si tende infatti ad usarle per indicare l'inizio e la fine delle sottoformule, com'è mostrato nella tabella \ref{tab:diff_par}.
\begin{table}[ht!]
    \centering
    \renewcommand{\arraystretch}{1.25}
    \begin{tabular}{ c  c } 
        \hline
        \textbf{Parlato} &  \textbf{Scritto} \\
        \hline
        \textit{a più b diviso c meno d} & (a + b) / (c - d)\\
        % \hline
        \textit{a più <PAUSA> b diviso c meno d <PAUSA>} & a + b / (c - d)\\
        % \hline
        \textit{a più b diviso c <PAUSA> meno d <PAUSA>} & (a + b) / c - d\\
        % \hline
        \textit{a più <PAUSA> b diviso c <PAUSA> meno d} & a + b / c - d\\
        \hline
    \end{tabular}
    \renewcommand{\arraystretch}{1}
    \caption{La tabella mostra alcuni esempi della differenza di parentesizzazione tra formule scritte e parlate.}
    \label{tab:diff_par}
\end{table}

Le pause sembrano adattarsi meglio al parlato piuttosto che le parentesi. Tuttavia è possibile comunque usare le parentesi per l'aggregazione delle formule. Si vedrà infatti nella sottosezione \ref{subsec:par_strat} che entrambe hanno pregi e difetti.    
\begin{notation}
    Siccome si è visto che parentesi e pause sono in un certo modo intercambiabili nel parlato, si adotterà il termine ``\textbf{parentesizzare}'' per indicare l'uso di parentesi o pause per aggregare una formula. Si è scelto quindi di usare le parentesi quadre per indicare in modo generale e più compatto l'aggregazione di una formula parlata, indifferentemente dal fatto che venga effettuata con parentesi tonde o pause. Dunque nel parlato la formula
    $$\text{<PAUSA> \textit{a più} <PAUSA> \textit{b diviso c meno d}}$$
    e la formula
    $$\text{(\textit{a più} ) \textit{b diviso c meno d}}$$
    sono entrambi rappresentabili più generalmente da 
    $$\text{[\textit{a più} ] \textit{b diviso c meno d}}$$
    % \begin{align*}
    %     &\text{\textit{<PAUSA>a più <PAUSA> b diviso c meno d}}\\
    %     &\text{\textit{(a più ) b diviso c meno d}}    
    % \end{align*}
    
\end{notation}

\subsection{Strategie di parentesizzazione}\label{subsec:par_strat}
Si è visto che per parentesizzare le formule nel parlato è possibile usare sia le pause che le parentesi. Entrambe le soluzioni mostrano pregi e difetti. Sono quindi state  pensate tre diverse strategie di aggregazione: 
\begin{description}
    \item [pause]: le pause sembrano una soluzione molto naturale ma se la formula presenta diversi livelli di parentesizzazione diventa difficile comprendere l'inizio e la fine di una sottoformula. 
    \item [parenthesis:] le parentesi permettono di capire con chiarezza l'inizio e la fine di una sottoformula ma sono particolarmente lunghe da pronunciare.
    \item [smart]: questo metodo è un approccio ibrido e cerca di combinare i lati positivi degli altri due metodi. Consiste nell'usare le pause al livello più interno della parentesizzazione e le parentesi nei restanti livelli in modo da avere una lettura abbastanza veloce e allo stesso comprensibile.    
\end{description}
  

\subsection{Estensione dell'aggregazione al resto degli operatori}\label{subsec:ext_aggr}
Fino ad ora sono state prese in considerazione soltanto le 4 operazioni, dato che sono gli operatori che generano più ambiguità. Per estendere il metodo di aggregazione a tutti gli altri operatori bisogna considerare che:
\begin{itemize}
    \item determinati operatori non prevedono alcun tipo di parentesizzazione. Ne sono un esempio gli operatori relazionali. Sono operatori che mettono in relazione due sottoformule; non vi è alcuna possibilità di ambiguità e quindi non necessitano mai di parentesizzazione.
    \item numeri e variabili non sono ambigui. Anch'essi non necessitano di parentesizzazione.
    \item tutti gli operatori prefissi agiscono su un'unica espressione. Tecnicamente non tutti gli operatori sono unari. Ad esempio la radice prende due operandi: il grado e l'espressione sotto di essa; la sommatoria prende tre operandi: il limite inferiore, quello superiore e l'espressione che ricade sotto di essa; etc. Ai fini dell'aggregazione però l'unico parametro che ha rilevanza è appunto l'espressione su cui agisce l'operatore, il quale è sempre unico. 
\end{itemize} 

Sotto queste considerazioni il metodo di aggregazione si semplifica e si riduce a due casi:
\begin{itemize}
    \item se l'espressione è un valore semplice, come un numero, una variabile o un altro operatore prefisso allora non serve parentesizzare in quanto non c'è ambiguità su cosa ricada all'interno dell'operatore e cosa no.
    \item se l'espressione è composita, ovvero è formata da operatori infissi, allora si procede a parentesizzare l'espressione.  
\end{itemize}
\begin{example}
    Considerando i seguenti esempi:
    \begin{itemize}
        \item ``\textit{radice quadrata di x}'': la radice non necessita di parentesizzazione in quanto il suo argomento è una variabile.
        \item ``\textit{radice quadrata di <PAUSA> x + 1 <PAUSA>}'': la radice necessita di parentesizzazione in quanto il suo argomento è una somma.
        \item ``\textit{limite per x tendente a più infinito di radice quadrata di <PAUSA> x + 1 <PAUSA>}'': il limite non necessita di parentesizzazione in quanto la radice è a sua volta un operatore infisso. La radice invece necessita di parentesizzazione in quanto il suo argomento è un'espressione composita. 
    \end{itemize}
    
\end{example}
\section{Algoritmo di Aggregazione}
L'idea su cui si basa per l'algoritmo di aggregazione è che durante la trasformazione da rappresentazione semantica a quella sintattica di una formula, si sfruttino implicitamente delle priorità degli operatori per stabilire se parentesizzare o meno una sotto formula. 

Verrà presentata inizialmente la versione dell'algoritmo per le formule scritte, in quanto è la notazione con cui si ha più familiarità, per poi mostrare come modificarla per ottenere l'algoritmo per le formule parlate. 
\subsection{Precedenza degli operatori}
\begin{wrapfigure}{r}{0.40\textwidth}
    \centering
    \begin{tabular}{|c|c|}
        \hline
        \textbf{Operatore} & \textbf{Priorità}\\
        \hline
         / & 4 \\
        \hline 
         * & 3 \\
        \hline
         - & 2 \\
        \hline
         + & 1 \\
        \hline
        \end{tabular}
    \caption{Tabella delle priorità degli operatori matematici}
    \label{fig:salience}
\end{wrapfigure}
 
Si è visto che nelle espressioni scritte, durante la valutazione, si considera che le moltiplicazioni e divisioni abbiano una priorità superiore a quelle delle sottrazioni e addizioni. Ciò lascia intendere che moltiplicazioni e divisioni abbiano la stessa priorità, così come le addizioni e sottrazioni. In realtà si è osservato che nella fase di generazione della rappresentazione sintattica di una formula, le divisioni e sottrazioni hanno rispettivamente una priorità maggiore delle moltiplicazioni e addizioni. Si crea così una gerarchia di priorità mostrata in figura {\ref{fig:salience}}. 

Intuitivamente la motivazione per cui si assegna alla divisione una priorità più alta rispetto alla moltiplicazione, e in modo del tutto analogo anche alle sottrazioni e addizioni, è dovuta principalmente alle proprietà di queste operazioni. La divisione, a differenza della moltiplicazione, non gode della proprietà associativa. Combinando questi due operatori si può vedere che    
$$a * b / c = (a * b) / c = a * (b / c)$$
e che quindi il risultato dell'espressione è corretto indifferentemente dall'ordine con cui eseguono le operazioni. Nel caso invece di 
$$a / b * c = (a / b) * c \neq a / (b * c)$$
si ottiene una valutazione corretta solo se si esegue la divisione prima della moltiplicazione. Risulta quindi che, anche se in alcuni casi sarebbe possibile valutare le operazioni in qualsiasi ordine,  nel caso generale eseguire prima le divisioni delle moltiplicazioni porta sempre a un risultato corretto.

Nella fase di generazione queste priorità possono quindi essere considerate come la precedenza che un operatore ha per accedere agli operandi adiacenti. In altre parole considerando come esempio 
$$a * b / c *d$$
si può affermare che la divisione ha la precedenza per accedere a $b$ e $c$ rispetto le altre due moltiplicazioni. 

\subsection{Algoritmo di aggregazione}
\begin{figure}[h]
    \begin{subfigure}[b]{.49\linewidth}
        \centering
        \begin{tikzpicture}[
            level 1/.style={sibling distance=5em},
            level 2/.style={sibling distance=5em},
            every node/.style={circle,draw,thick, minimum size=1cm},
            every label/.style={rectangle,draw,thick, minimum size=0.3cm}]
            \node [label={[label distance=0.1cm]30:2}]{-}
                child{node {a}}
                child{node [label={[label distance=0.1cm]30:3}] {*}
                    child{node[label={[label distance=0.1cm]20:4}]{/}
                        child{node{b}}
                        child{node{c}}
                    }
                    child{node{d}}
                };    
        \end{tikzpicture}    
        \caption{}
        \label{subfig:exp_tree_salience_a}
    \end{subfigure} %   
    \begin{subfigure}[b]{.49\linewidth}
        \centering
        \begin{tikzpicture}[
            level 1/.style={sibling distance=7em},
            level 2/.style={sibling distance=4em},
            every node/.style={circle,draw,thick, minimum size=1cm},
            every label/.style={rectangle,draw,thick, minimum size=0.3cm}]
            \node [label={[label distance=0.1cm]30:4}]{/}
                child {  node[label={[label distance=0.1cm]10:2}] {-}
                    child { node{a}}
                    child { node {b}}           
                }
                child {  node[label={[label distance=0.1cm]30:3}] {*}
                    child { node{c}}
                    child { node {d}}           
                };
        \end{tikzpicture}
        \caption{}
        \label{subfig:exp_tree_salience_b}
    \end{subfigure} 
    \caption{La figura \ref{subfig:exp_tree_a} rappresenta l'espressione $a - b / c * d$ mentre la figura \ref{subfig:exp_tree_b} rappresenta l'espressione $(a - b) / (c * d)$. I piccioli rettangoli in alto a destra dei nodi operatore indicano la loro priorità} 
    \label{fig:exp_tree_salience}
\end{figure}
Per descrivere il funzionamento dell'algoritmo di aggregazione si assume che il suo input sia un albero delle espressioni\footnote{Tecnicamente non si usa direttamente questo albero ma ai fini della spiegazione è del tutto ininfluente.}
etichettato con le priorità (si veda figura \ref{fig:exp_tree_salience}). % L'idea è quella che in un albero delle espressioni, se un nodo operatore ha come suo figlio un altro nodo operatore, 

L'idea è quella di visitare l'albero e per ogni nodo considerare la sua priorità e quella dei suoi figli. Se il nodo genitore ha una priorità più alta rispetto a quella di un nodo figlio allora significa che il nodo genitore ha la precedenza ad accedere all'operando che ha in comune con quel determinato figlio. Ciò però vorrebbe dire andare contro all'ordinamento delle operazioni definite dalla struttura dell'albero. Difatti un operatore che si trova a un livello più basso rispetto ad un altro dovrà per forza avere una priorità più alta sui suoi operandi. In questo caso si procede a parentesizzare tutta l'espressione che verrà generata dal nodo figlio in modo da assicurargli l'accesso ai sui operandi prima del nodo genitore. Se invece la priorità del nodo genitore è più bassa rispetto a quello del nodo figlio allora si sta rispettando il normale ordinamento imposto dall'albero e in questo caso la parentesizzazione non è necessaria.       
\begin{example}
    Si considerino i due alberi annotati riportati in figura \ref{fig:exp_tree_salience}. Si può osservare come tutti gli operatori dell'albero \ref{subfig:exp_tree_salience_a} hanno una priorità maggiore rispetto quella dei loro propri figli e che quindi l'espressione generata non necessità di parentesizzazione. 
    
    Si può invece osservare che l'operatore di divisione dell'albero \ref{subfig:exp_tree_salience_b} ha una priorità maggiore rispetto entrambi i suoi figli. È dunque necessario parentesizzare sia l'espressione generata dal nodo della moltiplicazione che quello della sottrazione.  
\end{example}
Fino a questo momento si è illustrato il metodo di aggregazione delle formule scritte composte esclusivamente dalla 4 operazioni. 

Per poter applicare lo stesso algoritmo alla matematica parlata bisogna considerare che, come indicato alla sottosezione \ref{subsec:spoken_priority}, la priorità degli operatori nel parlato sono inversi rispetto a quelli della scritto. Per questo motivo è possibile o invertire le priorità assegnate agli operatori oppure considerare la priorità più alta tanto quanto si avvicina allo zero. Si è optato per il secondo metodo in quanto prevede solamente di invertire il controllo per la parentesizzazione (da maggiore a minore). 

Per estendere invece l'algoritmo a tutti gli altri operatori bisogna considerare che:
\begin{itemize}
    \item tutti ciò che non prevede parentesizzazione non ha priorità. 
    \item tutti gli operatori prefissi hanno priorità 0, la più alta del parlato.  
\end{itemize} 

\begin{center}
    \begin{algorithm}[H]
        \SetAlgorithmName{Algoritmo}{algoritmo}{}
        \DontPrintSemicolon
        \SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
        \Input{\textit{math-phrase}, \textit{salience-parent}, \textit{salience-children}, \textit{aggregation-method}}
        \Output{\textit{math-phrase}}
        \BlankLine
        \eIf{salience-parent is not null {\bf and} salience-children is not null {\bf and} salience-parent $<$ salience-children}{
            \eIf{aggregation-method = parenthesis}{
                \KwRet{math-phrase wrapped between parenthesis}
            }{
                \KwRet{math-phrase wrapped between pauses}
            }
        }{
            \KwRet{math-phrase}}
        \caption{aggregazione}\label{lst:aggregazione}
    \end{algorithm}
\end{center}

Lo pseudocodice dell'algoritmo è mostrato nel listato \ref{lst:aggregazione}. Questo algoritmo viene applicato durante la visita dell'albero a tutti i nodi figli del nodo correntemente vistato. Viene preso in input il sintagma matematico generato dal nodo figlio, la precedenza del nodo genitore, quella del figlio e il metodo di aggregazione che può essere \emph{pause} oppure \emph{parenthesis}. Restituisce se necessario il sintagma matematico parentesizzato in accordo con il metodo di aggregazione o altrimenti lo stesso che ha preso in input.

L'algoritmo gestisce i due metodi di aggregazione \emph{pause} e \emph{parenthesis}. Il metodo \emph{smart} viene invece realizzato usando il metodo \emph{parenthesis} e applicando, tramite un approccio basato su espressioni regolari, la sostituzione delle parentesi più interne con delle pause.   

Si può notare come i controlli sulla priorità nulla permettano di evitare di parentesizzare tutti gli elementi che non ne hanno una associata. Inoltre grazie al fatto che la priorità degli operatori infissi è la maggiore di tutte, ovvero 0, è garantito che vengano parentesizzate soltanto le espressioni composite. Infine, dato che il controllo sulla priorità è effettuato con il \emph{minore stretto}, in caso di più operatori infissi annidati solamente quello più interno perentesizzerà se necessario l'espressione su cui è applicato.  
\section{Esempio di aggregazione}
Si consideri come esempio albero delle espressioni annotato con le priorità della formula $1 - ((a/2) + b)$:
\begin{figure}[ht!]
    \centering
    \begin{tikzpicture}[
        level 1/.style={sibling distance=5em},
        level 2/.style={sibling distance=5em},
        every node/.style={circle,draw,thick, minimum size=1cm},
        every label/.style={rectangle,draw,thick, minimum size=0.3cm}]
        \node [label={[label distance=0.1cm]30:2}]{-}
            child{node {1}}
            child{
                node [label={[label distance=0.1cm]30:1}] {+}
                child{
                    node[label={[label distance=0.1cm]20:4}]{/}
                    child{node{a}}
                    child{node{2}}
                }
                child{node{b}}
            };    
    \end{tikzpicture}    
    \caption{L'albero delle espressioni annotato con le priorità della formula $1 - ((a/2) +b)$.}
    \label{tikz:exp_tree_es_1}
\end{figure}
Sintetizzando l'albero e applicando l'algoritmo di aggregazione nelle tre forme viste si ottiene:
\begin{enumerate}
    \item \textbf{pause}: ``\textit{1 meno} <PAUSA><PAUSA> \textit{a diviso due} <PAUSA> \textit{più b}  <PAUSA> ''.
    \item \textbf{parenthesis}: ``\textit{1 meno} ( ( \textit{a diviso due} ) \textit{più b}  ) ''.
    \item \textbf{smart}: ``\textit{1 meno} ( <PAUSA> \textit{a diviso due} <PAUSA>\textit{più b}  ) ''.
\end{enumerate}
