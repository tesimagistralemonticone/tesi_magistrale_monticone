\chapter{Introduzione}\label{chap:intro}
Molti documenti scientifici contengono elementi, come formule, grafici e codice, che li rendono non completamente accessibili per chi possiede delle disabilità visive. Questo è dovuto al modo in cui questi elementi vengono letti. Difatti vengono sintetizzati mediante la lettura diretta del \LaTeX{} piuttosto che una loro lettura naturale.

Nel lavoro di questa tesi ci si è concentrati sull'accessibilità delle formule matematiche e si è sviluppato un software che permette la loro sintesi vocale naturale.

\section{Obiettivi}
Ai fini del lavoro di tesi ci si è posti come obiettivi:     
    \begin{enumerate}
        \item capire le problematiche della lettura di formule matematiche concentrandosi sulle esigenze dei non vedenti.
        \item stabilire se esiste una precisa struttura linguistica dietro le formule matematiche. 
        \item costruire un sistema di sintesi da formula matematica a Italiano.
        %  modellandolo come come una traduzione interlingua fatta da un'analisi e da una generazione (formula $\rightarrow$ Semantica $\rightarrow$ Italiano).
        % \item concentrarsi sulla parte di Semantica $\rightarrow$ Italiano che riguarda di più NLG.
        % \item costruire un sistema di sintesi che preveda l'aggiunta di un'ulteriore lingua, ovvero un sistema bilingue Italiano/Inglese.
        % \item (Da valutare) formula $\rightarrow$ Semantica.
    \end{enumerate}
\section{Panoramica del sistema}
    L'idea generale è quella di modellare il sistema come se fosse una traduzione interlingua fatta da un'analisi e da una generazione. Lo schema di traduzione può essere rappresentato con il triangolo di Vauquois (Hutchins \cite{HUTCHINS1995431} ) come riportato di seguito.
    \begin{center}
        \begin{tikzpicture}
            \tikzstyle{state}=[shape=rectangle, rounded corners,
            draw, align=center, minimum size=1cm, minimum width=3cm,
            top color=white, bottom color=blue!20]

            \node [state] (semantica) at (0,2.5) {Semantica};
            \node [state] (formula) at (-4,0) {Formula};
            \node [state] (italiano) at (4,0) {Italiano};
                
            \path[->,>=stealth]
            (formula)   edge [bend left=30] node[above=5, rotate=45] {Analisi}  (semantica) 
            (semantica) edge [bend left=30] node[above=5, rotate=315] {Generazione}  (italiano);
        \end{tikzpicture}
    \end{center}
    Durante lo svolgimento del lavoro di tesi si è posta enfasi sulla fase di generazione del parlato assumendo di avere a disposizione le formule già in una rappresentazione semantica. 

    È stato comunque necessario effettuare la fase di analisi per eseguire prove sul sistema ma ci si è appoggiati su uno strumento preesistente, ovvero \textit{LatexML}\cite{simplenlg-it}. 
    
    Il progetto è stato sviluppato in Clojure \cite{Hickey:2008:CPL:1408681.1408682}, un linguaggio funzionale compilato ed eseguito dalla \textit{Java Virtual Machine (JVM)}. Ha il vantaggio di essere funzionale ma al contempo di poter interagire con ogni tipo di libreria Java. Inoltre è altamente portabile in quanto può essere eseguito su qualsiasi dispositivo munito di \texttt{JVM}, la quale è ormai disponibile quasi ovunque. 

    Il software realizzato è stato rilasciato come open source sotto licenza GPL ed è consultabile al link \url{https://bitbucket.org/tesimagistralemonticone/formula-to-speech/src/master/}.
    
    Al momento la sintesi delle formule avviene solo in Italiano ma il sistema è stato concepito per essere multilingua. Difatti all'interno del sistema è codificata solamente la struttura linguistica delle formula mentre la lessicalizzazione italiana avviene tramite un dizionario esterno al sistema. È quindi possibile fornire un dizionario in una lingua diversa a patto che questa ammetta la stessa struttura linguistica delle formule. 
 

    Verranno ora presentate alcune delle scelte fatte riguardanti il sistema.  
    % I principali punti da chiarire riguardano le fasi di analisi e generazione. Verranno quindi discusse le varie opzione e problematiche relative a queste due fasi.
    \subsection{Formato dell'Input}
    Per prima cosa si è dovuto scegliere quale formalismo usare per le formule. Le due opzioni principali erano \LaTeX{} e \textit{Presentation MathML}. Entrambi i formalismi sono orientati alla presentazione e quindi non permettono una facile estrazione della semantica.  
    \begin{example}
        Si consideri come esempio la funzione identità $f(x) = x$. In un linguaggio orientato alla presentazione verrebbe considerata come la sequenza di simboli ``\textit{f, (, x, ), =, x}''. Si può notare che tale rappresentazione non indica cosa rappresentino i simboli che occorrono. Potrebbe trattarsi anche di una moltiplicazione tra la variabile $f$ e la variabile $x$. In un linguaggio semantico invece la formula verrebbe considerata come la funzione $f$ a cui è applicato il parametro $x$ e che restituisce $x$.   
    \end{example}
    Ai fini del sistema nessuno dei due formalismi presenta un vantaggio significativo rispetto all'altro. Si è quindi deciso di usare il \LaTeX per via della sua compattezza e diffusione. 

    Fra i possibili casi di studio da prendere in considerazione nel corso della tesi come testi di input vi sono:
    \begin{itemize}
        \item Il libro di analisi 1 di Pandolfi reperibile dal link \url{http://www.integr-abile.unito.it/Libri/Analisi1/html/analisi1.html} , da qui in poi chiamato \textit{Il pandolfi}.
        \item I tre volumi di \emph{The Feynman Lectures on Physics} reperibili dal link \url{http://www.feynmanlectures.caltech.edu/info/}
    \end{itemize} 
    Entrambi i libri sono liberamente consultabili e mettono a disposizione ogni formula sia in formato \latex che Presentation MathML. Si è scelto di usare come caso di studio il Pandolfi. 
   
    \subsection{Rappresentazione semantica}
    Fra i vari formalismi di rappresentazione della semantica di una formula quelli che spiccano sono \emph{Content MathML} e \emph{OpenMath}.
    \begin{description}
        \item[Content MathML] È un formato \texttt{XML} e permette di rappresentare la semantica tramite un insieme chiuso di tag. In altre parole la semantica di un simbolo di funzione viene rappresentato da un tag definito nel linguaggio. Risulta quindi molto semplice la fase di parsing ma al contempo non è possibile rappresentare la semantica di simboli di funzioni che non sono già presenti nella definizione del linguaggio.
        \begin{example}
            Ad esempio l'espressione $ a + b $ può essere rappresentata in \cmml{} come
            \begin{lstlisting}[style=xml]
    <apply>
        <plus/>
        <ci>a</ci>
        <ci>b</ci>
    </apply>
            \end{lstlisting} 
        \end{example} 
        \item[OpenMath] È simile a \textit{Content MathML} ma usa un dizionario estendibile che associa a ogni simbolo di funzione la sua semantica. Il dizionario è estendibile a piacere e quindi in linea di principio è possibile rappresentare la semantica di ogni simbolo di funzione.
        \begin{example}
            Ad esempio l'espressione $ a + b $ può essere rappresentata in \emph{Open Math} come
        \begin{lstlisting}[style=xml]
    <OMA>
        <OMS cd="arith1" name="plus"/>
        <OMV name="a"/>
        <OMV name="b"/>
    </OMA>
        \end{lstlisting} 
        \end{example}  
        \item[Content MathML esteso con OpenMath] È anche possibile combinare i due formalismi. Content MathML infatti permette di sfruttare \textit{OpenMath} per rappresentare i simboli di funzione non direttamente rappresentabili con \textit{Content MathML}.
        \begin{example}
            Ad esempio l'espressione $ a + b $ può essere rappresentata in \cmml{} esteso con \emph{Open Math} come
        \begin{lstlisting}[style=xml]
    <apply>
        <csymbol cd="arith1">plus</csymbol>
        <ci>a</ci>
        <ci>b</ci>
    </apply>
        \end{lstlisting} 
        \end{example}   
    \end{description}
    Come input del sistema si è scelto di usare il terzo formalismo ma con l'accortezza di trasformare i simboli di \textit{OpenMath} in dei veri e propri tag \texttt{XML} in moda da poter trattare l'input omogeneamente.
    
    \subsection{Architettura del sistema}
    L'architettura del software è composta da 4 moduli: 
    \begin{description}
        \item[LatexMl]: è il modulo che ha il compito di invocare lo strumento \emph{LatexML} passandogli in input la formula \LaTeX. L'output del modulo è la prima versione di \cmml{}.
        \item[PostProcessor]: questo modulo ha il compito di post processare il \cmml per renderlo compatibile con il sistema. Il suo output è la versione \emph{trattata} di \cmml{}. 
        \item[F-T-S](formula-to-speech): è il modulo che si occupa di generare la rappresentazione testuale della formula. Il suo output è un testo.
        \item[SynthCaller]: è il modulo con che ha il compito di invocare il sintetizzatore vocale e ottenere un file audio in formato \texttt{wav}.
    \end{description} 
    L'architettura nella sua interezza è rappresentata in figura \ref{tikz:arch}. 
    \begin{figure}[ht!]
        \centering
        \begin{tikzpicture}
            \tikzstyle{state}=[shape=rectangle, rounded corners,
            draw, align=center, minimum size=1cm, minimum width=2.5cm,
            top color=white, bottom color=blue!20]
            
            \node [state] (postprocessor) at (-2.5,3) {PostProcessor};
            \node [state] (latexml) at (-4,0) {LatexML};
            \node         (start) [left of=latexml,node distance=3cm, coordinate] {b};
            \node [state] (fts) at (1.5,3) {F-T-S};
            \node [state] (x) at (3,0) {SynthCaller};
            \node (end) [right of=x,node distance=3cm, coordinate] {b};
    
            \path[->] (start) edge node [above=5]{\LaTeX{}} (latexml);
            \path[->,>=stealth]
            (latexml)   edge [bend left=30] node[above=5, rotate=45] {cmml}  (postprocessor) 
            (postprocessor) edge [bend left=30] node[above=5] {cmml trattato}  (fts)
            (fts) edge [bend left=30] node[above=5, rotate=315] {testo}  (x)
            (x) edge node[above=5] {audio}  (end);
        \end{tikzpicture}
        \caption{L'immagine raffigura l'architettura del software.}
        \label{tikz:arch}
    \end{figure}
    
    % \begin{tikzpicture}[node distance=3.5cm,auto,>=latex']
    %     \node [int] (b) {\footnotesize{LatexML}};
    %     % \node (a) [left of=b,node distance=2cm, coordinate] {b};
    %     \node [int] (c) [right of=b] {\footnotesize{PostProcessor}};
    %     \node [int] (d) [right of=c] {\footnotesize{f-t-s}};
    %     \node [coordinate] (end) [right of=c, node distance=2cm]{};
    %     % \path[->] (a) edge node {\LaTeX{}} (b);
    %     \path[->] (b) edge node {cmml} (c);
    %     \draw[->] (c) edge node {$p$} (end) ;
    % \end{tikzpicture}

    \section{Principali problematiche}
    Le principali problematiche incontrate durante lo sviluppo del sistema riguardano sia la fase di analisi che quella di generazione. Verranno quindi discussi alcuni dei problemi più significativi riscontrati.  
    \subsection{Problematiche in fase di analisi}
    La notazione matematica è un linguaggio usato per trasmettere conoscenza matematica da persona a persona e che quindi la rende, per certi aspetti, simile ai linguaggi naturali. Difatti in questi ultimi si sfrutta l'ambiguità per formulare messaggi brevi e coincisi, e si conta sulle conoscenze dell'interlocutore per risolvere tali ambiguità. Allo stesso modo nella notazione matematica vi sono ambiguità che vengono risolte facilmente dal lettore ma che possono creare problemi in fase di analisi. Verranno quindi presentati i principali problemi che si incontrano durante questa fase, seguendo la presentazione di Kohlhase \cite{Kohlhase2008}.
    
    \subsubsection{Problema disambiguazione strutturale} 
    In alcuni casi la dalla presentazione di una formula è possibile estrarre direttamente la struttura della formula. Ad esempio da
    $$\frac{1}{\sqrt{2 \pi} \sigma}$$ 
    si evince facilmente, dalla lunghezza della barra, che l'argomento della radice è solamente $2\pi$. Purtroppo non sempre ciò è possibile. Ad esempio considerando 
    $$\sum_{i = 0}^n x_i + \alpha $$
    in assenza di perentesi si da per scontato che solamente $x_i$ ricada sotto la sommatoria. Considerando invece 
    $$\sum_{i = 0}^n x_i + y_i $$ 
    anche se le due sommatorie hanno la stessa struttura sintattica, si da per scontato che entrambi i termini ricadono sotto il simbolo di sommatoria in quanto $y_i$ è in funzione dell'indice $i$ della sommatoria. 
    \subsubsection{Problema di elisione-ricostruzione}
    Spesso i testi matematici ammettono ambiguità oppure omettono informazioni semantiche per permettere una sintassi meno verbosa e quindi più fluida da leggere. Si fa affidamento sulla conoscenza del dominio del lettore per disambiguare o ricostruire le informazioni mancanti. 
    \subsubsection{Il problema del contesto}
    La notazione matematica è dipendente dal contesto. Questo significa che uno stesso simbolo può assumere significati diversi in base al contesto in cui è usato. Si consideri come esempio la definizione di identità nel lambda calcolo tipato \emph{à la Church}(\cite{sep-type-theory-church}):
    $$\lambda x_\alpha. x =_\alpha \lambda y_\alpha. y \eqdef I^\alpha$$ 
    La prima e la terza occorrenza di $\alpha$ indicano il tipo di $x$ e $y$. La seconda invece fa parte del simbolo di $\alpha$-equivalenza che non ha niente a vedere con il tipo $\alpha$ di $x$ e $y$. L'ultima occorrenza invece indica che fra tutti i combinatori di identità possibili si seleziona quello che accetta un termine di tipo $\alpha$.          

    Un altro esempio rilevante è data dalla seguente formula:
    $$n\choose{k}$$
    In base al contesto in cui questa formula si trova potrebbe assumere almeno due diversi significati: la scelta di $k$ elementi da $n$ elementi totali, ovvero il coefficiente binomiale, oppure un vettore che contiene i valori $n$ e $k$.
    
    Inoltre nel caso in cui la formula si riferisca al coefficiente binomiale bisogna tenere conto che esistono diverse notazioni standard usate in vari Paesi: 
    $${n\choose{k}}, \; nC^k,\; C_k^n,\; C_n^k$$   
 
\subsection{Problematiche nella fase di generazione}
Una volta ottenuta la semantica di una formula bisogna trovare un modo efficacie per generare una sua rappresentazione testuale. Il compito non è banale in quanto si riscontrano problemi già con formule di piccole dimensioni. Per prima cosa bisogna chiarire quale notazione usare per descrivere semplici operazioni. Ad esempio la frazione
$$\frac{2}{3}$$
può essere letta in diversi modi:
\begin{itemize}
    \item \textit{due terzi}
    \item \textit{due diviso tre}
    \item \textit{due fratto tre}
    \item \textit{due su tre}
    \item \textit{due barra tre}
    \item \textit{A numeratore c'è: due. A denominatore c'è tre.}                    
\end{itemize}                      

Dopodiché bisogna considerare quale sia il modo più efficacie per aggregare parti di formula. Ad esempio come si dovrebbe procedere per distinguere le due seguenti formule?
$$x + \frac{c}{y}\qquad \text{e} \qquad  \frac{x + c}{y}$$  
Una delle possibilità è quella di usare un meccanismo basato sulle pause per delimitare blocchi di operazioni da eseguire. Ovvero:
\begin{itemize}
    \item $x + \dfrac{c}{y}$\qquad letto come \qquad \textit{x} \textless PAUSA\textgreater \textit{ più c fratto y}
    \item $\dfrac{x + c}{y} $\qquad letto come \qquad \textit{x  più c} \textless PAUSA\textgreater \textit{ fratto y}
\end{itemize}
\section{Struttura della tesi}
La tesi è articolata in diversi capitoli:
\begin{description}
    \item[Analisi linguistica delle formule matematiche]: in questo capitolo si accennerà com'è strutturato il linguaggio naturale e diversi formalismi per rappresentarne la struttura. Dopodiché si procederà a dare una descrizione delle \emph{frasi matematiche}, ovvero delle frasi in linguaggio naturale che rappresentano delle formule matematiche. Infine si definirà la struttura linguistica associata a ogni formula matematica.
    \item[Sintesi delle frasi matematiche]: in questo capitolo si discuterà sulla differenza tra il linguaggio parlato e quello scritto. Si presenteranno inoltre alcuni metodi alternativi per aggregare, ovvero scegliere come parentesizzare,  le formule matematiche e si concluderà presentando l'algoritmo in grado di fare ciò.
    \item[Design e Implementazione]: in questo capitolo si procederà a illustrare l'architettura del software realizzato discutendo come avvengono le fasi di analisi e di generazione.
    \item[Sperimentazione]: in questo capitolo verrà presentato il test effettuato per misurare l'efficacia del software realizzato e si analizzeranno i risultati.
    \item[Conclusione]: in questo capitolo si trarranno le conclusione e si elencheranno alcuni dei lavori che potrebbero essere fatti in futuro.      
\end{description} 

% \section{Obiettivi}
%     \begin{enumerate}
%         \item Capire le problematiche della lettura di formule matematiche concentrandosi sulle esigenze dei non vedenti.
%         \item Costruire un sistema di traduzione da formula matematica a Italiano. Modelliamo il sistema come una traduzione interlingua fatta da un'analisi e da una generazione (formula $\rightarrow$ Semantica $\rightarrow$ Italiano).
%         \item Concentrarsi sulla parte di Semantica $\rightarrow$ Italiano che riguarda di più NLG.
%         \item Costruire un sistema di sintesi bilingue Italiano/Inglese.
%         \item (Da valutare) formula $\rightarrow$ Semantica.
%     \end{enumerate}
% \section{Panoramica del problema}
%     L'idea generale è quella di trattare il problema come se fosse un problema di traduzione del linguaggio naturale a livello interlingua. Lo schema di traduzione può essere rappresentato con il triangolo di Vauquois come riportato di seguito.
%     \begin{center}
%         \begin{tikzpicture}
%             \tikzstyle{state}=[shape=rectangle, rounded corners,
%             draw, align=center, minimum size=1cm, minimum width=3cm,
%             top color=white, bottom color=blue!20]

%             \node [state] (semantica) at (0,2.5) {Semantica};
%             \node [state] (formula) at (-4,0) {Formula};
%             \node [state] (italiano) at (4,0) {Italiano};
                
%             \path[->,>=stealth]
%             (formula)   edge [bend left=30] node[above=5, rotate=45] {Analisi}  (semantica) 
%             (semantica) edge [bend left=30] node[above=5, rotate=315] {Generazione}  (italiano);
%         \end{tikzpicture}
%     \end{center}
%     I principali punti da chiarire riguardano le fasi di analisi e generazione. Verranno quindi discusse le varie opzione e problematiche relative a queste due fasi.
%     \subsection{Formato dell'Input}
%     Innanzitutto bisogna scegliere quale formalismo usare per le formule. Le due opzioni che al momento sono state prese in considerazione sono \latex e Presentation MathML. Entrambi i formalismi sono orientati alla presentazione e quindi non permettono una facile estrazione della semantica. 
%     Fra i possibili casi di studio da prendere in considerazione nel corso della tesi come testi di input vi sono:
%     \begin{itemize}
%         \item Il libro di analisi 1 di Pandolfi reperibile dal link \url{http://www.integr-abile.unito.it/Libri/Analisi1/html/analisi1.html} 
%         \item Le dispense del corso di Programmazione 1 del dipartimento di Informatica scritte da Roversi, Cardone e De Pierro. 
%         \item I tre volumi di \emph{The Feynman Lectures on Physics} reperibili dal link \url{http://www.feynmanlectures.caltech.edu/info/}
%     \end{itemize} 
%     Entrambi i libri sono liberamente consultabili e mettono a disposizione ogni formula sia in formato \latex che Presentation MathML. Inoltre esistono degli strumenti che permettono una traduzione da \latex a Presentation MathML in modo automatico, come ad esempio \emph{LateXML} \cite{millerlatexml} (da valutare ancora la precisione).
%     \textcolor{red}{Dragan: LateXML da problemi su Pandolfi (libro di Analisi I). Esempio: potenze: lascia la base fuori dal TAG}
%     \subsection{Rappresentazione semantica}
%     Fra i vari formalismi di rappresentazione della semantica di una formula quelli che spiccano sono \emph{Content MathML} e \emph{OpenMath}.
%     \begin{description}
%         \item[Content MathML] È un formato XML e permette di rappresentare la semantica tramite un insieme chiuso di tag. In altre parole la semantica di un simbolo di funzione viene rappresentato da un tag definito nel linguaggio. Risulta quindi molto semplice la fase di parsing ma al contempo non è possibile rappresentare la semantica di simboli di funzioni che non sono già presenti nella definizione del linguaggio.
%         \item[OpenMath] È simile a Content MathML ma usa un dizionario estendibile che associa a ogni simbolo di funzione la sua semantica. Il dizionario è estendibile a piacere e quindi in linea di principio è possibile rappresentare la semantica di ogni simbolo di funzione.
%         \item[Content MathML esteso con OpenMath] È anche possibile combinare i due formalismi. Content MathML infatti permette di sfruttare OpenMath per rappresentare i simboli di funzione non direttamente rappresentabili con Content MathML.
%     \end{description}
%     \subsection{Fase di analisi}
%     La notazione matematica è un linguaggio usato per trasmettere conoscenza matematica da persona a persona e che quindi lo rende, per certi aspetti, simile ai linguaggi naturali. Difatti in questi ultimi si sfrutta l'ambiguità per formulare messaggi brevi e coincisi, e si conta sulle conoscenze dell'interlocutore per risolvere tali ambiguità. Allo stesso modo nella notazione matematica vi sono ambiguità che vengono risolte facilmente dal lettore ma che possono creare problemi in fase di analisi. Verranno quindi presentati i principali problemi (\cite{Kohlhase2008}) che si incontrano durante questa fase.
    
%     \subsubsection{Problema disambiguazione strutturale} 
%     In alcuni casi la dalla presentazione di una formula è possibile estrarre direttamente la struttura della formula. Ad esempio da
%     $$\frac{1}{\sqrt{2 \pi} \sigma}$$ 
%     si evince facilmente, dalla lunghezza della barra, che l'argomento della radice è solamente $2\pi$. Purtroppo non sempre ciò è possibile. Ad esempio considerando 
%     $$\sum_{i = 0}^n x_i + \alpha $$
%     in assenza di perentesi si da per scontato che solamente $x_i$ ricada sotto la sommatoria. Considerando invece 
%     $$\sum_{i = 0}^n x_i + y_i $$ 
%     anche se le due sommatorie hanno la stessa struttura sintattica, si da per scontato che entrambi i termini ricadono sotto il simbolo di sommatoria in quanto $y_i$ è in funzione dell'indice $i$ della sommatoria. 
%     \subsubsection{Problema di elisione-ricostruzione}
%     Spesso i testi matematici ammettono ambiguità oppure omettono informazioni semantiche per permettere una sintassi meno verbosa e quindi più fluida da leggere. Si fa affidamento sulla conoscenza del dominio del lettore per disambiguare o ricostruire le informazioni mancanti. 
%     \subsubsection{Il problema del contesto}
%     La notazione matematica è dipendente dal contesto. Questo significa che uno stesso simbolo può assumere significati diversi in base al contesto in cui è usato. Si consideri come esempio la definizione di identità nel lambda calcolo tipato \emph{à la Church}:
%     $$\lambda x_\alpha. x =_\alpha \lambda y_\alpha. y \eqdef I^\alpha$$ 
%     La prima e la terza occorrenza di $\alpha$ indicano il tipo di $x$ e $y$. La seconda invece fa parte del simbolo di $\alpha$-equivalenza che non ha niente a vedere con il tipo $\alpha$ di $x$ e $y$. L'ultima occorrenza invece indica che fra tutti i combinatori di identità possibili si seleziona quello che accetta un termine di tipo $\alpha$.          

%     Un altro esempio rilevante è data dalla seguente formula:
%     $$n\choose{k}$$
%     In base al contesto in cui questa formula si trova potrebbe assumere almeno due diversi significati: la scelta di $k$ elementi da $n$ elementi totali, ovvero il coefficiente binomiale, oppure un vettore che contiene i valori $n$ e $k$.
    
%     Inoltre nel caso in cui la formula si riferisca al coefficiente binomiale bisogna tenere conto che esistono diverse notazioni standard usate in vari Paesi: 
%     $${n\choose{k}}, \; nC^k,\; C_k^n,\; C_n^k$$   
 
% \subsection{Problematiche nella fase di generazione}
% Una volta ottenuta la semantica di una formula bisogna trovare un modo efficacie per generare una sua rappresentazione testuale. Il compito non è banale in quanto si riscontrano problemi già con formule di piccole dimensioni. Per prima cosa bisogna chiarire quale notazione usare per descrivere semplici operazioni. Ad esempio la frazione
% $$\frac{2}{3}$$
% può essere letta in diversi modi:
% \begin{itemize}
%     \item \textit{due terzi}
%     \item \textit{due diviso tre}
%     \item \textit{due fratto tre}
%     \item \textit{due su tre}
%     \item \textit{due barra tre}
%     \item \textit{A numeratore c'è: due. A denominatore c'è tre.}                    
% \end{itemize}                      
% \textcolor{red}{Dragan: Siccome gli screen reader non hanno standard per la
%       lettura di piunteghgiatura e per le pause, dobbiamo gestirlo con un
%       ``normale'' text2speech.}


% Dopodiché bisogna considerare quale sia il modo più efficacie per aggregare parti di formula. Ad esempio come si dovrebbe procedere per distinguere le due seguenti formule?
% $$x + \frac{c}{y}\qquad \text{e} \qquad  \frac{x + c}{y}$$  
% Una delle possibilità è quella di usare un meccanismo basato sulle pause per delimitare blocchi di operazioni da eseguire. Ovvero:
% \begin{itemize}
%     \item $x + \dfrac{c}{y}$\qquad letto come \qquad \textit{x} \textless PAUSA\textgreater \textit{ più c fratto y}
%     \item $\dfrac{x + c}{y} $\qquad letto come \qquad \textit{x  più c} \textless PAUSA\textgreater \textit{ fratto y}
% \end{itemize}
% Un altro problema riscontrato è dovuto al fatto che nella notazione matematica alcuni operatori si pronunciano infissi mentre altri prefissi e non è semplice determinare in maniera generale quale sia la pronuncia corretta. Quello che si è osservato è che in Content MathML ogni operatore ha un'arietà (inteso come fattore di ramificazione dell'albero della formula). Ad esempio considerando la formula 
% $$x = \frac{-b \pm \sqrt{b^2 -4ac}}{2a}$$
% e il relativo Content MathML 
% \begin{lstlisting}[style=xml]
% <apply>
%     <eq/>
%     <ci>x</ci>
%     <apply>
%         <divide/>
%         <apply>
%             <csymbol>&#x00b1;</csymbol>       <!-- $\pm$ -->
%             <apply>
%                 <minus/>
%                 <ci>b</ci>
%             </apply>
%             <apply>
%                 <root/>
%                 <degree>
%                     <cn>2</cn>
%                 </degree>
%                 <apply>
%                     <minus/>
%                     <apply>
%                         <power/>
%                         <ci>b</ci>
%                         <cn>2</cn>
%                     </apply>
%                     <apply>
%                         <times/>
%                         <cn>4</cn>
%                         <ci>a</ci>
%                         <ci>c</ci>
%                     </apply>
%                 </apply>
%             </apply>
%         </apply>
%         <apply>
%             <times/>
%             <cn>2</cn>
%             <ci>a</ci>
%         </apply>
%     </apply>
% </apply>
% \end{lstlisting}
% Si possono distinguere principalmente 4 arietà diverse: 
% \begin{itemize}
%     \item \textbf{arietà 1}: sono i tag che rappresentano identificatori e numeri, indicati in arancione. Ad esempio 
%     \begin{lstlisting}[style=xml]
%     <cn>4</cn>
%     <ci>a</ci>
%     \end{lstlisting}
%     \item \textbf{arietà 2}: sono i blocchi di tag che rappresentano operatori unari. Ad esempio 
%     \begin{lstlisting}[style=xml]
%     <apply>
%         <minus/>
%         <ci>b</ci>
%     </apply>
%     \end{lstlisting}
%     In questi casi non ci sono problemi durante la generazione. L'operando è sempre (da quello che è stato osservato fino ad adesso) prefisso. Ad esempio $-b$ si pronuncerà sempre \textit{meno uno} e non \textit{uno meno}.
%     \item \textbf{arietà 3}: sono i blocchi di tag che rappresentano le funzioni binarie. Ad esempio
%     \begin{lstlisting}[style=xml]
%     <apply>
%         <power/>
%         <ci>b</ci>
%         <cn>2</cn>
%     </apply>
%     \end{lstlisting}
%     In questo caso, e molti altri, l'operando deve essere letto in forma infissa. Assumendo che la pronuncia della funzione \emph{power} si \textit{elevato} la pronuncia risulta essere \textit{b elevato due}. 
    
%     Purtroppo non tutti gli operatori binari si comportano in questo modo. Ad esempio 
%     \begin{lstlisting}[style=xml]
%     <apply>
%         <root/>
%         <degree>
%             <cn>2</cn>
%         </degree>
%         <ci>x</ci>
%     </apply>
%     \end{lstlisting}
%     rappresentante $\sqrt{x}$, ha la caratteristica di essere letta con l'operatore in forma prefissa, ovvero \textit{radice quadrata di x}. Bisogna quindi provare a trovare un modo efficiente per riuscire a distinguere i due casi in automatico. Sarebbe comunque possibile ricorrere a regole specifiche per questi casi ma il rischio è che il numero di esse cresca a dismisura.
%     \item \textbf{arietà $>$ 3}: L'unica caso osservato di arietà superiore a 3 riguarda le catene di moltiplicazioni come  
%     \begin{lstlisting}[style=xml]
%     <apply> 
%         <times/>
%         <cn>4</cn>
%         <ci>a</ci>
%         <ci>c</ci>
%     </apply>    
%     \end{lstlisting}
%     A meno di non trovare casi particolari, la pronuncia della formula in questo caso sembra abbastanza banale. Ad esempio si potrebbe semplicemente leggerle come una sequenza di operandi: \textit{quattro a c} 
% \end{itemize} 


% \section{Realizzazione della formula}
 
% \subsection{SimpleNLG-IT}



% \subsubsection{Struttura linguistica della formula}
% Come trattare le formule matematiche come oggetti linguistici? Sono frasi dichiarative oppure NP? Ad esempio ``due diviso tre'' sembra essere una frase dichiarativa dove due è l'subj e tre è l'obj (?), mentre ``due terzi'' sembra essere un NP dove due è un modificatore di terzi. Anche ``integrale da due a tre'' sembra essere un NP con due modificatori preposizionali (simile a ``il treno da Milano a Torino'').
% \subsection{Esempi} Per capire come affrontare il problema della rappresentazione delle formule si andranno a conisderare alcuni esempi tratti dal libro di pandolfi:
% \begin{example}
%     Si consideri come esempio  
%     $$A = \set{x \mid x > 0}$$ 
%     In linguaggio naturale potrebbe essere letta come  come ``\textit{A è uguale all'insieme di tutti gli x tali che x è maggiore di zero}''.
%     La rappresentazione in \emph{Content MathML} risulta essere la seguente
%     \begin{lstlisting}[style=xml]
%         <apply>
%             <eq/>
%             <ci>A</ci>
%             <apply>
%               <csymbol cd="latexml">conditional-set</csymbol>
%               <ci>x</ci>
%               <apply>
%                 <gt/>
%                 <ci>x</ci>
%                 <cn type="integer">0</cn>
%               </apply>
%             </apply>
%           </apply>
%     \end{lstlisting}    
%     Da questa rappresentazione è possibile ottenere in maniera quasi immediata un protoalbero a dipendenze semplicemente considerando il primo elemento di un blocco \emph{apply} come un nodo genitore mentre i restanti elementi come nodi figli. Il protoalbero a dipendenza risulta quindi essere :
%     \begin{center}
%         \begin{tikzpicture}[
%             level 1/.style={sibling distance=10em},
%             % level 2/.style={sibling distance=5em}
%             every node/.style={ellipse,draw,thick}
%             ]
%             \node {\textbf{\xml{eq}}}
%                 child {  node {\textbf{\xml{ci}}}
%                     child { node{\textit{A}}}
%                     edge from parent node[left,draw=none] {\textit{subj}}
%                 }
%                 child {  node {\textbf{\xml{cond-set}}}
%                     child { node{\textbf{\xml{ci}}}
%                         child { node{\textit{x}}}
%                         edge from parent node[left,draw=none] {\textit{subj}}
%                     }
%                     child { node {\textbf{\xml{gt}}}
%                         child { node {\textbf{\xml{ci}}}
%                             child { node {\textit{x}}}
%                             edge from parent node[left,draw=none] {\textit{subj}}
%                         }
%                         child { node {\textbf{\xml{cn}}}
%                             child { node {0}}
%                             edge from parent node[right,draw=none] {\textit{obj}}
%                         }
%                         edge from parent node[right,draw=none] {\textit{obj}}
%                     }           
%                     edge from parent node[right,draw=none] {\textit{obj}}
%                 }
%                 ;
%         \end{tikzpicture}
%     \end{center}    
% \end{example}   

% \begin{example}
%     La formula 
%     $$\lim_{x\to+\infty}f(x)=+\infty$$
%     può essere letta in linguaggio naturale come ``\textit{il limite per x tendente a più infinito di f di x è uguale a più infinito}''
%     mentre il suo \textit{Content MathML} è il seguente: 
%     \begin{lstlisting}[style=xml]
%         <apply>
%             <eq/>
%             <apply>
%               <apply>
%                 <subscript/>
%                 <limit/>
%                 <apply>
%                   <ci>$\rightarrow$</ci>
%                   <ci>x</ci>
%                   <apply>
%                     <plus/>
%                     <infinity/>
%                   </apply>
%                 </apply>
%               </apply>
%               <apply>
%                 <ci>f</ci>
%                 <ci>x</ci>
%               </apply>
%             </apply>
%             <apply>
%               <plus/>
%               <infinity/>
%             </apply>
%           </apply>
%     \end{lstlisting}   

% \end{example}

% \subsubsection{Dizionario matematico}
% Lessico standard: \url{http://ceur-ws.org/Vol-1749/paper35.pdf}

% Bisogna capire come gestire il vocabolario matematico (es. terzi o
% integrale).  SimpleNLG-IT di default usa il Nuovo Vocabolario di Base
% di De Mauro (
% \url{https://www.internazionale.it/opinione/tullio-de-mauro/2016/12/23/il-nuovo-vocabolario-di-base-della-lingua-italiana}.).
% Ci sono due possibilità da indagare: 1. costruirlo manualmente 2. estrarlo in automatico da manuali latex o dal pandolfi stesso.
% Altra cosa da capire, e se il vocabolario matematico sostituisce o integra il dizionari di default.

% \subsection{Problematiche Generali}
% \begin{itemize}
%     \item La notazione $$g(x)=f_{|_{D}}(x)$$  usata in ``2.2.5 Limiti di restrizioni di funzioni e limiti direzionali'' del libro di analisi 1 può portare problematiche in fase di generazione. Da approfondire.
%     \item La notazione $$k\mapsto n(k)$$ usata ``2.4.1 Le sottosuccessioni e i loro limiti'' del libro di analisi può essere letta come ``\dots che a $k$ associa $n(k)$''.
%     \item Da indagare su come bisogna comportarsi con il verbo ``bi-implicare''. Deve essere trattato come un nuovo verbo o semplicemente come il verbo ``implicare'' unito al prefisso ``bi-''?
%     \item ``\textit{funzione segno}'' deve essere trattato come un termine atomico? In alternativa quale POS dovrebbe avere ``\textit{segno}''?
%     \item Come è possibile differenziare le lettere maiuscole da quelle minuscole in linguaggio naturale? 
% \end{itemize}
% \subsection{Possibili assunzioni}
% \begin{itemize}
%     \item La $C$, esempio $$f \in C^{2}(a,b)$$ ``\textit{f appartiene alla classe delle funzioni derivabili due volte nell'intervallo a b}'',  potrebbe essere associata a ``\textit{classe}''.
%     \item La $D$, esempio $$D_{x_{0}}\left(f(x)+g(x)\right)=f^{\prime}(x_{0})+g^{\prime}(x_{0})$$, potrebbere essere associata a ``\textit{derivata}''.
%     \item La $F$, esempio $$F(x)=G(x)+c$$ potrebbe essere assegnata a ``\textit{primitiva}''.
%     \item $d$ può essere indicare la derivata i il termine differenziale in un integrale  
% \end{itemize}

% \section{Progressi}
% Qui verranno annotati tutti i progressi effettuati sul lavoro di tesi.
% \begin{enumerate}
%     \item Provare subito a fare un prototipino software sulle prime 5 formule per capire le problematiche
% \end{enumerate}
% \section{LatexML}
% Casi in cui latexml non risulta corretto e necessità di intervento umano:
% \begin{description}
%     \item [Applicazione di funzione:] $f(x)$ viene considerata come se fosse una moltiplicazione fra $f$ e $x$.
%     \item [Coppie:] in $\set{(x,y) \mid x > 0, y > 0}$ la coppia $(x,y)$ viene considerata come un intervallo aperto.
%     \item [Elevamento a potenza:] $x^2$ viene considerato in modo puramente sintattico come ``\textit{x apice due}'' invece che in maniera semantica ``\textit{x elevato due}''. 
%     \item [Operatori mancanti:] insiemi condizionali (\emph{conditional-set}), bi implicazione  (\emph{iff})
% \end{description} 
% \section{TODO list}
% \begin{itemize}
%     \item gestire le funzioni definite per casi (medio)
%     \item gestire simboli :empyset, lettere greche... (medio, t2s di ibm non sembra leggere correttamente le lettere greche in esadecimale)
%     \item gestire le derivate parziali (facile) 
%     \item gestire dominio, codominio  immagine (difficile, in latex si indica come testo ma in contentml esiste)
%     \item gestire i logaritmi (facile)
%     \item gestire positivo e negativo / sinistro destro (medio, non sempre rappresentato allo stesso modo)
%     \item gestire funzione inversa (facile)
%     \item gestire coefficiente binomiale (facile)
%     \item gestire coniugato complesso
%     \item investigare sulla natura linguistica di per-ogni e esiste. Sono np o vp o altro?
%     \item investigare sul case delle variabili. Tipo $a \in A$
% \end{itemize}
% Tempi di lettura: 
%    solo di queste formule o di altre nel libro

% 5 formule facili 
%     5 IBM+smart 
%     2 espeak+smart

% 5 formule difficili
%    5 con IBM+par
%    5 con IBM+pause
%    5 con IBM+smart
%    1 con espeak+par
%    1 con espeak+smart
%    1 con espeak+smart