\chapter{Design e Implementazione}\label{chap:implementazione}
Come già introdotto nel capitolo \ref{chap:intro}, la sintesi delle formule matematiche è suddivisa in due fasi principali: quella di analisi e quella di generazione. 
\section{Fase di analisi del \LaTeX{}}
Lo scopo di questa fase è quello di ottenere, partendo da una formula \LaTeX, una sua rappresentazione in \cmml{}. Si procederà a dare una breve descrizione del \cmml{} per poi illustrarne la generazione. 
\subsection{Content MathML} 
Il \cmml{} è una rappresentazione semantica espressa mediante il linguaggio \texttt{XML} in cui i tag rappresentano operatori matematici o entità di base, come numeri, identificatori, gradi, variabili legate, etc.
Ad esempio i numeri e gli indicatori sono espressi da tag unari, come mostrato nel seguente listato 
\begin{lstlisting}[style=xml]
    <cn>4</cn>
    <ci>a</ci>
\end{lstlisting}
In \cmml{} tutti gli operatori matematici sono espressi in forma prefissa. Ad esempio la radice può essere rappresentata in forma prefissa come la funzione binaria \textit{root} che prende in input il grado e l'espressione a cui viene applicata:
$$\sqrt[n]{x} = \textit{root}(n,x)$$
la quale in \cmml{} può essere rappresentata come
\begin{lstlisting}[style=xml]
<apply>
    <root/>
    <degree><ci>n</ci></degree>
    <ci>x</ci>
</apply>
\end{lstlisting}
dove il tag \texttt{apply} indica l'applicazione dei parametri ad una funzione.

Un altro esempio significativo è fornito dalla sommatoria. La sua rappresentazione in forma prefissa consiste nella funzione \textit{sum} che prende in input la variabile legata, il limite inferiore, il limite superiore e l'espressione a cui viene applicata:
$$\sum_{i=0}^{n}{x} = \mathit{sum}(i, 0, n, x)$$
la quale in \cmml{} viene rappresentata come 
\begin{lstlisting}[style=xml]
<apply>
    <sum/>
    <bvar><ci>i</ci></bvar>
    <lowlimit><cn>0</cn></lowlimit>
    <uplimit><cn>0</cn></uplimit>
    <ci>x</ci>
</apply>
\end{lstlisting}    
\subsection{Produzione del Content MathML}
La produzione del \cmml{} è stata effettuata grazie a uno strumento preesistente chiamato \textit{LatexML} \cite{millerlatexml}. Questo software prende in input una formula \latex e restituisce in output una sua rappresentazione in \cmml{}. Sfortunatamente il risultato ottenuto non è sempre ciò che ci si aspetterebbe. Difatti in alcuni casi, per via di dell'ambiguità delle formule \latex, si ottiene una rappresentazione che non corrisponde con la formula di partenza. Ne è un chiaro esempio l'ambiguità tra l'applicazione e la moltiplicazione. In questo caso l'espressione $f(x)$ viene tradotta come
\begin{lstlisting}[style=xml]
<apply>
    <times/>
    <ci>f</ci>
    <ci>x</ci>
</apply>
\end{lstlisting}    
In questo particolare caso, assumendo che il simbolo $f$ sua usato esclusivamente per indicare una funzione si può semplicemente rimuovere il tag della moltiplicazione ed ottenere il risultato desiderato. 

Un altro esempio è quello dell'ambiguità tra coppia e intervallo, cioè $[a,b]$, che si crea quando viene usato una coppia in un insieme condizionale. Difatti in questo contesto l'unico elemento ammesso è la coppia.

In ogni caso il \cmml{} ottenuto non è direttamente usabile dal sistema in quanto ogni tag è preceduto dal suffisso \texttt{m:}, come mostrato nel listato \ref{lst:cmml_pre} . Inoltre tutti gli operatori non definiti in \cmml{} non vengono rappresentati con dei tag ma indicati all'interno del generico tag \texttt{csymbol}. Si preferirebbe però poter lavorare su una struttura omogenea in cui ogni operatore è considerato come un tag.

Si è quindi realizzato uno script che permette di post-processare il \cmml{} ottenuto da \textit{LatexML} per renderlo compatibile con il sistema.
\newpage
\begin{example}
    Si consideri l'insieme condizionale $A = \set{x \mid x > 0}$. L'output ottenuto da \textit{LatexML} è il seguente
    \begin{lstlisting}[style=xml, caption=\cmml{} prodotto da LatexML, captionpos=b, label=lst:cmml_pre]
<m:apply>
    <m:eq/>
    <m:ci>A</m:ci>
    <m:apply>
        <m:csymbol cd="latexml">conditional-set</m:csymbol>
        <m:ci>x</m:ci>
        <m:apply>
            <m:gt/>
            <m:ci>x</m:ci>
            <m:cn type="integer">0</m:cn>
        </m:apply>
    </m:apply>
</m:apply>      
    \end{lstlisting}    
mentre dopo la fase di post-processamento si ottiene
\begin{lstlisting}[style=xml, caption=\cmml{} trattato dal PostProcessor, captionpos=b]
<apply>
    <eq/>
    <ci>A</ci>
    <apply>
        <conditional-set/>
        <ci>x</ci>
        <apply>
            <gt/>
            <ci>x</ci>
            <cn type="integer">0</cn>
        </apply>
    </apply>
</apply>      
        \end{lstlisting}    
\end{example}      
% Schema progetto, librerie usate, moduli etc...
% Clojure \cite{Hickey:2008:CPL:1408681.1408682}
\section{Fase di Generazione della Frase Matematica}
Lo scopo di questa fase è quella di generare il parlato delle formule partendo dalla loro rappresentazione in \cmml{}. Ciò viene effettuato costruendo l'albero linguistico della formula e poi trasformandolo in linguaggio naturale con l'ausilio della libreria \textit{SimpleNLG-IT}. 
\subsection{L'albero linguistico}
Nella sezione \ref{sec:lan_structure} è stato mostrato che ogni categoria di operatori possiede una propria struttura linguistica. All'interno di ogni categoria però ciascun operatore è caratterizzato da determinati elementi lessicali. 
% Questi elementi rappresentano le foglie degli alberi \textit{SimpleNLG} 
% Gli operatori che possono occorrere nelle formule vengono rappresentati in formato Json. In particolare si è scelto di associare ad ogni operatore un insieme di proprietà rappresentanti elementi lessicali che cooccorrono insieme ad esso nel linguaggio naturale. 
% Gli operatori che occorrono nelle formule matematiche possono essere tradotti in linguaggio naturale in modo quasi diretto. In particolare si è osservato che la traduzione può essere eseguita considerando un insieme di elementi lessicali associati a ogni operatore.

Ad esempio considerando l'operatore ``$\geq$'' si può notare come la sua traduzione nel sintagma ``\textit{è maggiore o uguale a}'' è costituita da:
\begin{itemize}
    \item il verbo \emph{essere} che regge l'operatore
    \item i due aggettivi, \emph{maggiore} e \emph{uguale}, coordinati con la congiunzione \emph{o}
    \item la preposizione \emph{a} necessaria a legare il sintagma con il resto della frase
 \end{itemize}
Questi elementi lessicali non sono altro che le foglie degli alberi \textit{SimpleNLG} mostrati nella sezione \ref{sec:lan_structure}. 

L'idea è dunque quella di collezionare queste e altre informazioni in un dizionario scritto in formato Json da consultare durante la fase di creazione dell'albero linguistico.

Ad esempio le informazioni più rilevanti dell'operatore ``$\geq$'' sono rappresentati dalle seguenti proprietà Json: 
\begin{lstlisting}[style=json] 
    "geq": {  
        "name": "maggiore",
        "coordination-conj": "o",
        "coordinated-name": "uguale",
        "preposition": "a", 
        "verb": "essere",
    }
\end{lstlisting}
\subsubsection{Proprietà degli Operatori}
Non tutti gli operatori necessitano le stesse proprietà. Si procederà quindi a darne una descrizione dettagliata:
\begin{description}
    \item [name:] indica il nome principale dell'operatore. Ad esempio ``\textit{valore}'' in ``\textit{valore assoluto di}'' o ``\textit{maggiore}'' in ``\textit{è maggiore o uguale a}''.
    \item [coordinated-name:] indica un eventuale nome coordinato al nome principale come ad esempio ``\textit{uguale}'' in ``\textit{è maggiore o uguale a}''.
    \item [coordination-conj:] indica quale congiunzione usare in caso vi sia una coordinazione di nomi. Ad esempio ``\textit{o}'' in ``\textit{è maggiore o uguale a}''.
    \item [preposition:] indica quale preposizione usare per legare il sintagma corrente con il resto della frase. Ad esempio ``\textit{a}'' in ``\textit{è maggiore o uguale a}'' oppure ``\textit{di}'' in ``\textit{maggiore di}''.   
    \item [preposition-alt:] indica una preposizione alternativa a quella normale. Viene usata in rari casi quando l'operatore, in un determinato contesto, richiede una preposizione diversa da quella di default. Ad esempio l'operatore di limite inferiore viene in genere usato per indicare il valore da cui parte una sommatoria, una produttoria o un integrale e richiede la preposizione ``\textit{da}'', come ad esempio nella frase ``\textit{sommatoria per x \textbf{da} zero a \dots} ''. Lo stesso operatore però viene usato anche per indicare il valore a cui tende un limite e in questo particolare caso la proposizione richiesta è ``\textit{a}'' come ad esempio nella frase ``\textit{limite per x tendente \textbf{a} zero}''. 
    \item [preposition-sub:] indica quale preposizione usare per legare il sintagma con un eventuale frase subordinata. Ad esempio in ``\textit{Il limite per x tendente a più infinito di f di x}'', la frase subordinata risulta essere ``\textit{x tendente a più infinito}'' e la proposizione che la lega alla frase principale è ``\textit{per}''.
    \item [post-modifier:] indica l'eventuale post modificatore del nome. Ad esempio ``\textit{assoluto}'' in ``\textit{valore assoluto di}''.
    \item [verb:] indica quale verbo regge l'operatore. Ad esempio ``\textit{essere}'' in ``\textit{è maggiore o uguale a}'' oppure ``\textit{tendere}'' in ``\textit{il limite per x tendente a più infinito di f di x}''.
    \item [determiner:] indica se l'operatore necessita di articolo oppure no. Ad esempio la sommatoria può essere letta in modo più naturale preceduta dell'articolo, mentre l'operatore \emph{più} invece no. È comunque solo un valore di default e può essere ignorato all'occorrenza.
    \item [is-negate:] specifica se l'operatore è negato oppure no. Ad esempio nella frase ``\textit{x non appartiene all'insieme A}'' questo valore è impostato a \emph{true}.
    \item [is-plural:] specifica se l'operatore è plurale oppure no. Ad esempio nella frase ``\textit{l'insieme degli x tali che x è maggiore di zero}'' questo valore è impostato a \emph{true}.
    \item [is-past-participle:] specifica se il verbo della frase matematica associata all'operatore è al participio passato. Ad esempio nella frase ``\textit{a elevato a 2}'' questo valore è impostato a \emph{true}. Se questo valore è impostato a \emph{false} si assume che il verbo sia al presente. Fanno eccezione i verbi usati nei limiti inferiori, i quali sono sempre impostati al participio presente (``\textit{x tendente a \dots}'', ``\textit{x appartenente a \dots}'').
    \item [category:] indica la categoria a cui appartiene l'operatore. 
\end{description}
    
\subsubsection{Costruzione dell'albero linguistico}
A questo punto si è a conoscenza della struttura linguistica di ogni categoria di operatori e degli elementi lessicali associati a ogni operatore. Ciò che rimane è la costruzione dell'albero nella sua interezza. Per fare ciò si trasforma il \cmml{} in una struttura ad albero per agevolare la sua navigazione. Dopodiché partendo dalla radice si genera l'albero linguistico considerando l'operatore e i suoi operandi. Nel caso un operando sia a sua volta un operatore si applica ricorsivamente il procedimento su di esso fino a giungere alle foglie dell'albero. 

\subsection{Realizzazione del parlato}
La trasformazione dell'albero linguistico in linguaggio naturale si effettua grazie al modulo di realizzazione della libreria \textit{SimpleNLG-IT}. Questo modulo per generare una frase svolge diversi compiti: 
\begin{itemize}
    \item esegue un ordinamento dei i vari sintagmi dell'albero in base al loro legame di dipendenza. Ad esempio in italiano si fa precedere il soggetto al verbo e il verbo all'oggetto. 
    \item coniuga i verbi al tempo specificato. Se nessuno è specificato assume che il tempo sia il presente.
    \item effettua la flessione degli aggettivi in genere e numero in accordo con quello del nome a cui sono associati.
    \item effettua la flessione degli nomi in genere e numero se viene esplicitamente richiesto di farlo.
    \item sceglie la corretta versione degli articoli in base al nome a cui sono associati.
    \item nega la frase nel caso venga esplicitamente richiesto di farlo. 
\end{itemize}
Fra i compiti appena elencati, quelli che prevedono variazioni morfologiche necessitano di un particolare dizionario che specifica il lessico usato. In caso di lessemi regolari, la libreria è in grado di effettuare queste variazioni in autonomia. Ad esempio se si specifica un verbo regolare, la libreria è in grado di eseguire la coniugazione. Se invece i lessemi prevedono qualche forma irregolare devono essere specificati manualmente.

Nel contesto delle formule matematiche è stato necessario estendere il dizionario italiano usato di base di \textit{SimpleNLG-IT} con un altro dizionario contenente le parole matematiche, come ad esempio ``(\textit{integrale}, N)'', ``(\textit{derivata}, N)'' \dots   

\subsection{Esempio di sintesi di una formula}
% Si procedera adesso a fornire un esempio di generazione di una semplice formula matematica.
% Si consideri una formula
Si prenda nuovamente in considerazione la formula dell'esempio \ref{tikz:exp_tree_es_1} 
$$1 - ((a/2) +b)$$
Per effettuare la sintesi della formula per prima cosa viene data in input al tool \emph{LatexML} e il \cmml{} ottenuto è il seguente:
\begin{lstlisting}[style=xml]
    <m:apply>
        <m:minus/>
        <m:cn type="integer">1</m:cn>
        <m:apply>
            <m:plus/>
            <m:apply>
                <m:divide/>
                <m:ci>a</m:ci>
                <m:cn type="integer">2</m:cn>
            </m:apply>
            <m:ci>b</m:ci>
        </m:apply>
    </m:apply>
\end{lstlisting}
Dopodiché su usa il modulo di post processamento per trattare il \cmml{} ottenuto. Il risultato ottenuto è il seguente:
\begin{lstlisting}[style=xml]
    <apply>
        <minus/>
        <cn type="integer">1</cn>
        <apply>
            <plus/>
            <apply>
                <divide/>
                <ci>a</ci>
                <cn type="integer">2</cn>
            </apply>
            <ci>b</ci>
        </apply>
    </apply>
\end{lstlisting}
A questo punto il \cmml{} viene trasformato in una struttura ad albero nel linguaggio di programmazione clojure. Questa rappresentazione è però molto verbosa e poco leggibile. Inoltre è del tutto equivalente come struttura al \cmml{}\footnote{Il \cmml{} in quanto \texttt{XML} è a tutti gli effetti un albero. Può essere visto come un albero che cresce verso destra. Ogni livello di indentazione dei tag rappresenta un livello di profondità.}. Si preferisce quindi usare una rappresentazione più semplice da leggere, ovvero la seguente rappresentazione ad albero del \cmml{}:
\begin{center}
    \begin{forest}
        for tree={draw, ellipse, s sep=1.2cm} 
        [\xml{minus} 
            [\xml{cn}
                [1]
            ]
            [\xml{plus}
                [\xml{divide}
                    [ci
                        [a]
                    ]
                    [cn
                        [2]
                    ]
                ]
                [ci
                    [b]
                ]
            ]
        ]
    \end{forest}
\end{center}
Una volta ottenuta questa struttura ad albero si procede a generare l'albero linguistico creando ricorsivamente la struttura linguistica associata ad ogni operatore. L'albero ottenuto è il seguente:

\begin{center}
    \begin{forest} 
        % for tree={s sep=20}  
        [Clause, calign=center
            [NP, no edge, name=subj1
                [\textit{1}]
            ]
            [V [\textit{meno}]]
            [Clause, no edge, name=obj1
                [Clause, no edge, name=subj2
                    [NP, no edge, name=subj3
                        [\textit{a}]
                    ]
                    [V [\textit{diviso}]]
                    [NP, no edge, name=obj3
                        [\textit{2}]
                    ]
                ]
                {
                    \draw[->, blue] () to[out=south,in=north] (subj3) node [anchor=south east,align=center] {subj};
                    \draw[->, blue] () to[out=south,in=north] (obj3) node [anchor=south west,align=center] {obj};
                 }
                [V [\textit{più}]]
                [NP, no edge, name=obj2
                    [\textit{b}]
                ]
            ]
            {
                \draw[->, blue] () to[out=west,in=north] (subj2) node [anchor=south east,align=center] {subj};
                \draw[->, blue] () to[out=east,in=north] (obj2) node [anchor=south west,align=center] {obj};
            }
        ]
        {
            \draw[->, blue] () to[out=west,in=north] (subj1) node [anchor=south east,align=center] {subj};
            \draw[->, blue] () to[out=east,in=north] (obj1) node [anchor=south west,align=center] {obj};
        }
    \end{forest}
\end{center}
Infine sfruttando l'algoritmo di aggregazione, ad esempio con la strategia parenthesis, e il modulo del realizer di \textit{SimpleNLG-IT} si ottiene la frase matematica ``\textit{1 -((a/2) + b)}''.