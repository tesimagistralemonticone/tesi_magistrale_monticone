\documentclass[xcolor=dvipsnames]{beamer}

\usepackage{amssymb, amsmath, tikz, listings, enumerate, forest, tabularx}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[italian]{babel}
\usepackage[lined, ruled]{algorithm2e}

\usetikzlibrary{arrows}
\usetikzlibrary{shapes}

% \definecolor{smoke}{RGB}{200,200,255}

\title[Sintesi di formule matematiche]{Sintesi vocale di formule matematiche in linguaggio naturale tramite tecniche di NLG }
\author[Michele Monticone]{\texorpdfstring{Candidato: Michele Monticone\\Relatore: Prof. Alessandro Mazzei}{Michele Monticone}}
\date[12/05/2019]{12 Aprile 2019 - Tesi di Laurea Magistrale}
\titlegraphic{\includegraphics[width=2cm]{./Immagini/unito_logo_red.png}}
\institute[Unito]{Università degli studi di Torino}

\usetheme{CambridgeUS}
\usecolortheme{beaver}

\useoutertheme{infolines}
\usecolortheme{whale}
\usecolortheme{orchid}

\definecolor{beamer@blendedblue}{rgb}{0.137,0.466,0.741}

\setbeamercolor{structure}{fg=beamer@blendedblue}
\setbeamercolor{titlelike}{parent=structure}

% \usepackage{helvet}
% \usefonttheme{professionalfonts}
% \usecolortheme[rgb={0.2,0.5,0.5}]{structure}

\setbeamercovered{invisible}
\setbeamercovered{%
  again covered={\opaqueness<1->{15}}}

\newcommand{\eqdef}{\;\hat{=}\;}

\lstdefinestyle{general}{
    mathescape=true,
    numbers=left,
    showstringspaces=false,
    framesep=10pt,
    xleftmargin=26pt,xrightmargin=10pt,
    frame=single,
    framexleftmargin=1.5em,
    columns=flexible,
    basicstyle={\linespread{1}\small\ttfamily},
    numberstyle=\tiny\color{gray},
    keywordstyle=\color{blue},
    stringstyle=\color{ForestGreen},
    commentstyle=\itshape\color{gray},
    extendedchars=true,
    literate={ù}{{\`u}}1
}

\lstdefinestyle{xml}{
    style=general,
    language=XML,
    keywordstyle = [2]{\color{MidnightBlue}},
    keywordstyle = [3]{\color{orange}},
    keywordstyle = [4]{\color{TealBlue}},
    morekeywords = [2]{eq,  divide, root, power, times, degree,minus, gt, lt, subscript, plus, minus, infinity, limit, sum, bvar, lowlimit, uplimit, apply, conditional, set ,m:eq, m:csymbol, m:gt, m:apply, m:divide, m:plus, m:minus, prosody, speak},
    morekeywords = [3]{ci, cn, m:ci, m:cn},
    morekeywords = [4]{csymbol}
}

\lstdefinestyle{json}{
    style=general,
    keywordstyle = [2]{\color{Orange}},
    keywordstyle = [3]{\color{Purple}},
    keywordstyle = [4]{\color{ForestGreen}},
    morekeywords = [2]{true, false},
    morekeywords = [3]{geq,determiner, name, coordination, conj, coordinated, preposition, pre, post, modifier, verb, is, plural, negate, sub, category},
    morekeywords = [4]{maggiore, minore, uguale, a, o, essere, rel, op}
}

\newcolumntype{Y}{>{\centering\arraybackslash}X}

\AtBeginSection[]{\begin{frame}{Tabella dei contenuti}\tableofcontents[currentsection]\end{frame}}

\begin{document}

\begin{frame}
        \maketitle
\end{frame}
\section{Introduzione}
% \subsection{Sintesi vocale}
\begin{frame}
    \frametitle{Sintesi vocale}
    \begin{block}{Cosa si intende con sintesi vocale di una formula matematica?}
        La sintesi di formule matematiche in linguaggio naturale consiste nella generazione di una rappresentazione testuale e vocale della formula a partire dalla sua rappresentazione \LaTeX{}.
    \end{block}
    \pause
    \begin{block}{Osservazione}
        La rappresentazione testuale di una formula è un linguaggio prettamente parlato. È comune leggere a parole ``\textit{x più uno}'' mentre non lo è scriverlo. Quindi alcuni fenomeni che normalmente non hanno uno status linguistico, come le pause, in questo dominio lo assumono.    
    \end{block}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Esempio}
    \begin{block}{Formula matematica}
        $$f(x) = \frac{1}{\sqrt{x}}$$
    \end{block}
    \pause
    \begin{block}{Rappresentazione \LaTeX}
        \begin{lstlisting}[style=general]
\begin{equation}
    f(x) = \frac{1}{\sqrt{x}}
\end{equation}
        \end{lstlisting}
    \end{block}
    \pause
    \begin{block}{Rappresentazione naturale}
        \centering
        ``\textit{f di x è uguale a 1 diviso la radice quadrata di x}''
    \end{block} 
\end{frame}

\begin{frame}
    \frametitle{Motivazioni}
    \begin{block}{Il problema}
            I documenti scientifici contenenti formule matematiche non sono completamente accessibili per chi ha della disabilità visive.   
    \end{block}
    \pause
    \begin{block}{Stato dell'arte}
        Il metodo di riferimento per la lettura delle formule matematiche è la lettura del codice \LaTeX{}. Ma 
        \begin{itemize}
            \item è verboso e quindi non efficiente
            \item non tutti lo conoscono
        \end{itemize}
    \end{block}
\end{frame}
\begin{frame}
    \frametitle{Motivazioni}
    \begin{block}{Obiettivi}
        \begin{enumerate}
            \item Capire le problematiche della lettura di formule matematiche concentrandosi sulle esigenze dei non vedenti
            \item Stabilire se esiste una precisa struttura linguistica dietro le formule matematiche
            \item Costruire un sistema di sintesi da formula matematica a Italiano
        \end{enumerate}
    \end{block}
\end{frame}
% \begin{frame}{Tabella dei contenuti}
%     \tableofcontents[Introduzione]
% \end{frame}
\section{Schematizzazione del processo di sintesi}
\begin{frame}{Schematizzazione del processo di sintesi}
    \begin{block}{Modello del sistema}
        Il sistema è modellato come se fosse una traduzione interlingua fatta da un'analisi e da una generazione.
        \begin{center}
            \begin{tikzpicture}
                \tikzstyle{state}=[shape=rectangle, rounded corners,
                draw, align=center, minimum size=1cm, minimum width=3cm,
                top color=white, bottom color=blue!20]

                \node [state] (semantica) at (0,2.5) {Semantica};
                \node [state] (formula) at (-4,0) {Formula};
                \node [state] (italiano) at (4,0) {Italiano};
                    
                \path[->,>=stealth]
                (formula)   edge [bend left=30, color=orange] node[above=5, rotate=45] {Analisi}  (semantica) 
                (semantica) edge [bend left=30, color=blue] node[above=5, rotate=315] {Generazione}  (italiano);
            \end{tikzpicture}
        \end{center}
    \end{block}
\end{frame}
\begin{frame}
    \frametitle{Rappresentazione delle formule}
    \begin{block}{Rappresentazione sintattica}
        $$f(x) = x + 1$$
    \end{block}
    \pause
    \begin{block}{Rappresentazione semantica}
        \begin{center}
            \begin{forest}
                for tree={draw, circle, minimum size=7mm} 
                [\text{=}
                    [@
                        [$f$]
                        [$x$]
                    ]
                    [+
                        [$x$]
                        [$1$]
                    ]
                ]
            \end{forest}
        \end{center}
    \end{block}
\end{frame}

\section{Problematiche generali}
\begin{frame}{Problematiche in fase di analisi}
    \begin{block}{Problema di elisione-ricostruzione}
        \begin{enumerate}
            \item Omettere informazioni semantiche per avere una notazione più compatta
            \item Confidare nella conoscenza del dominio dell'interlocutore per la ricostruzione delle informazioni mancanti 
        \end{enumerate}
    \end{block}
    \pause
    \begin{block}{Esempio}
        Applicazione o moltiplicazione?
            \begin{align*}
                \visible<3->{\tau_a x(t) &=} x(t - a)  \qquad \visible<4>{\text{applicazione!}}\\
                \visible<3->{f(x) &=} x(n - 1) \qquad \visible<4>{\text{moltiplicazione!}}
            \end{align*}
    \end{block}
\end{frame}    
\begin{frame}{Problematiche in fase di analisi}
    \begin{block}{Problema disambiguazione strutturale}
        Non è sempre possibile determinare la struttura di una formula dalla sua presentazione
    \end{block}
    \pause
    \begin{block}{Esempio}
        \begin{align*}
            &\sum_{i=0}^{n}x_{i}+\alpha \visible<3>{= \left(\sum_{i=0}^{n}x_{i}\right)+\alpha}\\
            &\sum_{i=0}^{n}x_i+y_i \visible<3>{= \left(\sum_{i=0}^{n}(x_i+y_i)\right)} 
        \end{align*}
    \end{block}
\end{frame}    

\begin{frame}{Problematiche in fase di analisi}
    \begin{block}{Problema del contesto}
        Lo stesso simbolo può assumere significati diversi in base al contesto in cui si trova
    \end{block}
    \pause
    \begin{block}{Esempio}
        Funzione identità:
        $$\lambda x_{\only<3>{\color{TealBlue}}\alpha}. x =_{\only<4>{\color{TealBlue}}\alpha} \lambda y_{\only<3>{\color{TealBlue}}\alpha}. y \eqdef I^{\only<5>{\color{TealBlue}}\alpha}$$ 
        \begin{itemize}
            \item <3> {\color{TealBlue}$\alpha$} indica il tipo di $x$ e $y$
            \item <4> {\color{TealBlue}$\alpha$} fa parte del simbolo di $\alpha$-equivalenza   
            \item <5> {\color{TealBlue}$\alpha$} indica che fra tutti i combinatori di identità possibili si seleziona quello che accetta un termine di tipo $\alpha$
        \end{itemize}
    \end{block}
\end{frame}    

\begin{frame}{Problematiche in fase di generazione}
    \begin{block}{Molteplici forme di lettura}
        La stessa formula può essere letta in diversi modo in linguaggio naturale
    \end{block}
    \pause
    \begin{block}{Esempio}
        La frazione $$\frac{2}{3}$$
        può essere letta come
        \begin{itemize}
            \item \textit{due terzi}
            \item \textit{due diviso tre}
            \item \textit{due fratto tre}
            \item \textit{due su tre}
            \item \textit{A numeratore c'è due. A denominatore c'è tre.}        
        \end{itemize}
    \end{block}
\end{frame}    

\begin{frame}{Problematiche in fase di generazione}
    \begin{block}{Problema di aggregazione}
        La lettura di una formula deve preservare la sua struttura
    \end{block}
    \pause
    \begin{block}{Esempio}
        Come distinguere la lettura delle due formule
        $$x + \frac{c}{y}\qquad \text{e} \qquad  \frac{x + c}{y}$$  
    \end{block}
\end{frame}    

\section{Analisi linguistica}
\begin{frame}{Linguaggio naturale e notazione matematica}
    \begin{block}{Notazione matematica}
        È un linguaggio simbolico scritto concepito per rappresentare concetti matematici. 
    \end{block}
    \pause
    \begin{block}{Linguaggio naturale}
        È un linguaggio basato sulle parole usato per la comunicazione tra esseri umani.
    \end{block}
    \pause
    \begin{block}{Frasi matematiche}
        La notazione matematica viene usata nel parlato come se fosse un linguaggio naturale.  
    \end{block}
\end{frame}    

% \begin{frame}{Linguaggio naturale: rappresentazione a costituenti}
%     \begin{block}{Costituente}
%         Un costituente o anche detto sintagma è un gruppo di parole rappresentante una porzione di frase.
%     \end{block}
%     \pause
%     \begin{block}{Sintagmi}
%         \begin{description}
%             \item[S](\textit{sentence}): indica la frase nella sua interezza.
%             \item[NP] (\textit{noun phrase}): indica un sintagma nominale, ovvero una parte di frase che contiene un nome.    
%             \item[VP] (\textit{verbal phrase}): indica un sintagma verbale, ovvero una parte di frase che contiene un verbo.
%             \item[PP] (\textit{prepositional phrase}): indica un sintagma preposizionale, ovvero una parte di frase che contiene una preposizione. 
%         \end{description}  
%     \end{block}
% \end{frame}    
% \begin{frame}{Linguaggio naturale: rappresentazione a costituenti}
%     \begin{block}{Parti del discorso}
%         \begin{description}
%             \item[Det] (\textit{determiner}): indica un articolo.
%             \item[N] (\textit{noun}): indica un nome comune. 
%             \item[Adj](\textit{adjective}): indica un aggettivo.
%             \item[Adv](\textit{adverb}): indica un avverbio.
%             \item[V](\textit{verb}): indica un verbo.
%             \item[P](\textit{preposition}): indica una preposizione.    
%         \end{description}  
%     \end{block}
% \end{frame}  

% \begin{frame}[fragile]{Linguaggio naturale: rappresentazione a costituenti}
%     \begin{block}{Grammatica libera dal contesto}
%         \begin{lstlisting}[style=general]
%     // Regole grammaticali
%     S  $\rightarrow$ NP VP | $\dots$
%     NP $\rightarrow$ Det N | $\dots$  
%     VP $\rightarrow$ V PP | $\dots$
%     PP $\rightarrow$ P NP | $\dots$

%     // regole lessicali
%     Det $\rightarrow$ il | lo | la | i | gli | le
%     N   $\rightarrow$ cane | palla | $\dots$
%     V   $\rightarrow$ gioca | $\dots$
%     P   $\rightarrow$ di | a | da | in | con |$\dots$ 
%         \end{lstlisting}  
%     \end{block}
% \end{frame}    
% \begin{frame}[fragile]{Linguaggio naturale: rappresentazione a dipendenze}
%     \begin{block}{Dipendenza}
%         Una dipendenza è il legame che intercorre tra due parole
%     \end{block}
%     \begin{block}{Grammatica libera dal contesto}
%         \begin{lstlisting}[style=general]
%     // Regole grammaticali
%     S  $\rightarrow$ NP VP | $\dots$
%     NP $\rightarrow$ Det N | $\dots$  
%     VP $\rightarrow$ V PP | $\dots$
%     PP $\rightarrow$ P NP | $\dots$

%     // regole lessicali
%     Det $\rightarrow$ il | lo | la | i | gli | le
%     N   $\rightarrow$ cane | palla | $\dots$
%     V   $\rightarrow$ gioca | $\dots$
%     P   $\rightarrow$ di | a | da | in | con |$\dots$ 
%         \end{lstlisting}  
%     \end{block}
% \end{frame}    

% \begin{frame}[fragile]{Linguaggio naturale: rappresentazione a costituenti}
%     \begin{block}{Esempio di albero sintattico}
%         \begin{center}
%             \begin{forest}
%                 [S
%                     [NP
%                         [Det
%                             [\textit{il}]
%                         ]
%                         [N
%                             [\textit{cane}]
%                         ]
%                     ]
%                     [VP
%                         [V
%                             [\textit{gioca}]
%                         ]
%                         [PP
%                             [P
%                                 [\textit{con}]
%                             ]
%                             [NP
%                                 [Det
%                                     [\textit{la}]
%                                 ]
%                                 [N
%                                     [\textit{palla}]
%                                 ]
%                             ]
%                         ]
%                     ]
%                 ]
%             \end{forest}     
%         \end{center}
%     \end{block}
% \end{frame}    

\begin{frame}{Struttura linguistica delle formule}
    \begin{itemize}
        \item La struttura linguistica di una formula è costruita a partire dalla struttura degli operatori che la compongono. 
        \pause
        \item Ma come stabilire qual è la struttura linguistica di un operatore?
    \end{itemize}
    \pause
    \begin{block}{Alberi linguistici di esempio}
        \begin{columns}
            \begin{column}{0.4\textwidth}
                \centering
                \begin{tabular}{ c | c } 
                    % \hline
                    \textbf{Notazione M.}  & \textbf{Frase M.} \\
                    \hline
                    $a + b$ & \textit{a più b}\\
                    % \hline
                \end{tabular}
                \begin{center}
                    \begin{forest}  
                        [Clause, calign=center
                            [NP, no edge, name=subj
                                [\textit{a}]
                            ]
                            [VP
                                [\textit{più}]]    
                            [NP, no edge, name=obj
                                [\textit{b}]
                            ]  
                        ]{
                            \draw[->, blue] () to[out=west,in=north] (subj) node [anchor=south east,align=center] {subj};
                            \draw[->, blue] () to[out=east,in=north] (obj) node [anchor=south west,align=center] {obj};
                        }
                    \end{forest}
                \end{center}
            \end{column}
            \visible<4>{
                \begin{column}{0.4\textwidth}
                    \centering
                    \begin{tabular}{ c | c } 
                        % \hline
                        \textbf{Notazione M.}  & \textbf{Frase M.} \\
                        \hline
                        $a - b$ & \textit{a meno b}\\
                        % \hline
                    \end{tabular}
                    \begin{center}
                        \begin{forest}  
                            [Clause, calign=center
                                [NP, no edge, name=subj
                                    [\textit{a}]
                                ]
                                [VP
                                    [\textit{meno}]]    
                                [NP, no edge, name=obj
                                    [\textit{b}]
                                ]  
                            ]{
                                \draw[->, blue] () to[out=west,in=north] (subj) node [anchor=south east,align=center] {subj};
                                \draw[->, blue] () to[out=east,in=north] (obj) node [anchor=south west,align=center] {obj};
                            }
                        \end{forest}
                    \end{center}
                \end{column}
            }
        \end{columns}
        
    \end{block}
\end{frame}

\begin{frame}{Struttura linguistica delle formule}
    Operatori simili hanno struttura linguistica simile. Possono infatti essere suddivisi un categorie 
    \pause
    \begin{block}{Categorie degli operatori}
        \begin{itemize}
            \item operatori relazionali
            \item operatori aritmetici, algebrici e insiemistici 
            \item connettivi logici
            \item operatori ausiliari 
            \item sequenze
            \item funzioni elementari
            \item calculus  
            \item insieme condizionale  
            \item coppia
        \end{itemize}     
    \end{block}
\end{frame}    

\begin{frame}{Struttura linguistica degli operatori relazionali}
    % \begin{block}{Frase matematica}
        \centering
        \begin{tabular}{ c | c } 
            % \hline
            \textbf{Notazione Matematica}  & \textbf{Frase Matematica} \\
            \hline
            $ x\ll 0$ & \textit{x è molto minore di 0}\\
            % \hline
        \end{tabular} 

    % \end{block}
    % \begin{block}{Albero linguistico di esempio}
        \begin{center}
            \begin{forest}  
                [Clause, calign=center
                    [NP, no edge, name=subj
                        [\textit{x}]
                    ]     
                    [VP
                        [\textit{essere}]]    
                    [Adjp, no edge, name=obj
                        [AdvP, no edge, name=premod
                            [\textit{molto}]
                        ]
                        [\textit{minore}]
                    ]{\draw[->, blue] () to[out=west,in=north] (premod) node [anchor=south east,align=center] {pre\\mod};}  [PP, no edge, name=comp
                        [\textit{di}]
                        [NP
                            [\textit{0}]
                        ]
                    ]  
                ]{
                    \draw[->, blue] () to[out=west,in=north] (subj) node [anchor=south east,align=center] {subj};
                    \draw[->, blue] () to[out=east,in=north] (obj) node [anchor=south west,align=center] {obj};
                    \draw[->, blue] () to[out=east,in=north] (comp) node [anchor=south west,align=center] {comp};
                }
            \end{forest}
        \end{center}
    % \end{block}
\end{frame}
\begin{frame}{Struttura linguistica delle sequenze ternarie}
    % \begin{block}{Frase matematica}
        \centering
        \begin{tabular}{ c | c } 
            % \hline
            \textbf{Notazione Matematica}  & \textbf{Frase Matematica} \\
            \hline
            $ \displaystyle{\lim_{x \to 0} f(x)}$ & \textit{il limite per x tendente a 0 di f di x}\\
            % \hline
        \end{tabular} 

    % \end{block}
    % \begin{block}{Albero linguistico di esempio}
        \begin{center}
            \begin{forest} 
                % for tree={s sep=20}  
                [NP, calign=center
                    [\textit{il}]
                    [\textit{limite}]
                    [PP, no edge, name=pp0
                        [\textit{per}]
                        [Clause
                            [NP, no edge, name=subj
                                [\textit{x}]
                            ]
                            [V
                                [\textit{tendere}]
                            ]
                            [PP, no edge, name=obj
                                [\textit{a}]
                                [NP
                                    [\textit{0}]
                                ]
                            ]
                        ]
                        {
                            \draw[->, blue] () to[out=west,in=north] (subj) node [anchor=south west,align=center] {subj};
                            \draw[->, blue] () to[out=east,in=north] (obj) node [anchor=south west,align=center] {obj};
                        }
                    ]
                    [PP, no edge, name=pp1
                        [\textit{di}]
                        [NP
                            [\textit{f}]
                            [PP, no edge, name=pp2
                                [\textit{di}]
                                [NP
                                    [\textit{x}]
                                ]
                            ]
                        ]
                        {
                            \draw[->, blue] () to[out=south,in=north] (pp2) node [anchor=south west,align=center] {complement};
                        }
                    ]
                ]
                {
                    \draw[->, blue] () to[out=south,in=north] (pp0) node [anchor=south west,align=center] {complement};
                    \draw[->, blue] () to[out=east,in=north] (pp1) node [anchor=south west,align=center] {complement};
                }
            \end{forest}
        \end{center}
    % \end{block}
\end{frame}
\section{Sintesi delle formule matematiche}

\begin{frame}{Realizzazione della formula}
    L'albero linguistico viene trasformato in linguaggio naturale grazie al modulo di realizzazione della libreria software \textit{SimpleNLG-IT}.
    \pause
    \begin{block}{SimpleNLG-IT}
        \begin{itemize}[<+->]
            \item Esegue un ordinamento dei i vari sintagmi dell'albero in base al loro legame di dipendenza
            \item Coniuga i verbi al tempo specificato
            \item Effettua la flessione degli aggettivi in genere e numero in accordo con quello del nome a cui sono associati
            \item Effettua la flessione degli nomi in genere e numero se viene esplicitamente richiesto di farlo
            \item Sceglie la corretta versione degli articoli in base al nome a cui sono associati
            \item Nega la frase nel caso venga esplicitamente richiesto di farlo 
        \end{itemize}
    \end{block}  
\end{frame}

\begin{frame}{Linearizzazione degli alberi delle espressioni}
    \begin{block}{Alberi delle espressioni}
        \begin{columns}
            \begin{column}{0.4\textwidth}
                $$a / (b + c) $$
                \centering
                \begin{tikzpicture}[
                    level 1/.style={sibling distance=5em},
                    % level 2/.style={sibling distance=5em}
                    every node/.style={circle,draw,thick, minimum size=.8cm}
                    ]
                    \node {/}
                        child {  node {a}}
                        child {  node {+}
                            child { node{b}}
                            child { node {c}}           
                        };
                \end{tikzpicture}    
            \end{column}
            \begin{column}{0.4\textwidth}
                $$(a / b) + c $$
                \begin{tikzpicture}[
                    level 1/.style={sibling distance=5em},
                    % level 2/.style={sibling distance=5em} 
                    every node/.style={circle,draw,thick, minimum size=.8cm}
                    ]
                    \node {+}
                        child {  node {/}
                            child { node{a}}
                            child { node {b}}           
                        }
                        child {  node {c}};
                \end{tikzpicture} 
            \end{column}
        \end{columns}
    \end{block}
    \pause
    \begin{block}{Linearizzazione dei due alberi}
        Entrambi gli alberi sono linearizzati come ``\textit{a diviso b più c}''
    \end{block}
\end{frame}

\begin{frame}{Aggregazione delle formule}
    Per preservare l'ordine delle operazioni bisogna parentesizzarle
    \begin{block}{Albero dell'espressione}
        \begin{center}
            \begin{tikzpicture}[
                level 1/.style={sibling distance=10em, level distance=1cm},
                level 2/.style={sibling distance=5em},
                every node/.style={circle,draw,thick, minimum size=.8cm, level distance=1cm}
                ]
                \node {+}
                    child {  node {a}}
                    child {  node {-}
                        child { node{/}
                        child {  node {c}}
                        child {  node {d}}
                        }
                        child { node {d}}           
                    };
            \end{tikzpicture}
        \end{center}
    \end{block}
    \begin{block}{Formula parentesizzata}
        \begin{equation}
            (a + ((b / c) - d))
        \end{equation}
    \end{block}
\end{frame}

\begin{frame}{Alleggerire la notazione}
    \begin{itemize}
        \item Si vuole avere una notazione meno verbosa \pause
        \item Può essere fatto introducendo delle regole sulla valutazione degli operatori\pause
        \begin{itemize}
            \item Valutare da sinistra verso destra
            \item Eseguire prima le divisioni e moltiplicazioni
            \item Infine sottrazioni e addizioni          
        \end{itemize}\pause
        \item Oppure in modo equivalente \pause
        \begin{itemize}
            \item Eseguire prima le divisioni
            \item poi le moltiplicazioni
            \item poi le sottrazioni
            \item infine le addizioni          
        \end{itemize}
    \end{itemize}
    \pause
    \begin{block}{Esempio}
        La formula $(a + ((b / c) - d))$ può essere riscritta come $a + b / c - d$
    \end{block}
\end{frame}
\begin{frame}{Notazione matematica VS frasi matematiche}
    \begin{block}{Pause}
        Nelle frasi matematiche sembrano più indicate le pause rispetto le parentesi.
    \end{block}
    \pause
    \begin{block}{Osservazione}
       Pronunciando una formula supponiamo che si assegnino implicitamente delle priorità che non corrispondono con quelle usate nella notazione matematica. 
    \end{block}
    \pause
    \begin{block}{Esempio}
        Come ipotesi di lavoro ho formalizzato la percezione di
        $$\text{``\textit{x più 1 diviso x meno 2}''}$$ 
        come
        $$\frac{x + 1}{x - 2}$$
    \end{block}
\end{frame}
\begin{frame}{Notazione matematica VS frasi matematiche}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{block}{Priorità nel parlato}
                Le priorità degli operatori nel parlato sono opposte a quelle dello scritto
            \end{block}
        \end{column}
        \begin{column}{0.4\textwidth}
            \centering
            \begin{tabular}{c c}
                \hline
                \textbf{Operatore} & \textbf{Priorità}\\
                \hline
                 + & 4 \\
                % \hline 
                 - & 3 \\
                % \hline
                 * & 2 \\
                % \hline
                 / & 1 \\
                \hline
                \end{tabular}
        \end{column}
    \end{columns}
    \pause
    \begin{block}{Esempio}
        \centering
        \renewcommand{\arraystretch}{1.25}
        \begin{tabular}{ c  c } 
            \hline
            \textbf{Frase M.} &  \textbf{Notazione M.} \\
            \hline
            \textit{a più b diviso c meno d} & (a + b) / (c - d)\\
            % \hline
            \textit{a più} <P> \textit{b diviso c meno d} <P> & a + b / (c - d)\\
            % \hline
            \textit{a più b diviso c} <P> \textit{meno d} <P> & (a + b) / c - d\\
            % \hline
            \textit{a più} <P> \textit{b diviso c} <P> \textit{meno d} & a + b / c - d\\
            \hline
        \end{tabular}
        \renewcommand{\arraystretch}{1}
    \end{block}
\end{frame}
\begin{frame}{Algoritmo di aggregazione}
    % \begin{block}{Alberi annotati con le priorità degli operatori}
        \begin{columns}
            \begin{column}{0.3\textwidth}
                \centering
                \begin{tikzpicture}[
                    level 1/.style={sibling distance=5em},
                    level 2/.style={sibling distance=5em},
                    every node/.style={circle,draw,thick, minimum size=0.8cm},
                    every label/.style={rectangle,draw,thick, minimum size=0.3cm}]
                    \node [label={[label distance=0.1cm]30:3}]{-}
                        child{node {a}}
                        child{node [label={[label distance=0.1cm]30:2}] {*}
                            child{node[label={[label distance=0.1cm]20:1}]{/}
                                child{node{b}}
                                child{node{c}}
                            }
                            child{node{d}}
                        };    
                \end{tikzpicture}    
            \end{column}
            \begin{column}{0.7\textwidth}
              \begin{small}
                  
                
            
            \begin{algorithm}[H]
                \SetAlgorithmName{Algoritmo}{algoritmo}{}
                \DontPrintSemicolon
                \SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
                \Input{\textit{math-phrase}, \textit{salience-parent}, \textit{salience-children}, \textit{aggregation-method}}
                \Output{\textit{math-phrase}}
                \BlankLine
                \eIf{salience-parent is not null {\bf and} salience-children is not null {\bf and} salience-parent $<$ salience-children}{
                    \eIf{aggregation-method = parenthesis}{
                        \KwRet{math-phrase wrapped between parenthesis}
                    }{
                        \KwRet{math-phrase wrapped between pauses}
                    }
                }{
                    \KwRet{math-phrase}}
                \caption{aggregazione}\label{lst:aggregazione}
            \end{algorithm}
        \end{small}
        \end{column}
            
            % \visible<3->{
            %     \begin{column}{0.5\textwidth}
            %         \centering
            %         \begin{tikzpicture}[
            %             level 1/.style={sibling distance=7em},
            %             level 2/.style={sibling distance=4em},
            %             every node/.style={circle,draw,thick, minimum size=0.8cm, level distance=1cm},
            %             every label/.style={rectangle,draw,thick, minimum size=0.3cm}]
            %             \node [label={[label distance=0.1cm]30:1}]{/}
            %                 child {  node[label={[label distance=0.1cm]10:3}] {-}
            %                     child { node{a}}
            %                     child { node {b}}           
            %                 }
            %                 child {  node[label={[label distance=0.1cm]30:2}] {*}
            %                     child { node{c}}
            %                     child { node {d}}           
            %                 };
            %         \end{tikzpicture}            
            %     \end{column}
            % }
        \end{columns}
    % \end{block}
    \pause
    \begin{enumerate}
        \item \textit{a meno} <P> <P> \textit{b diviso c} <P> \textit{per d} <P>
        % \item <4> \textit{a meno b diviso c per d}   
    \end{enumerate}
\end{frame}
\begin{frame}{Strategie di aggregazione}
    Sono state testate tre strategie di aggregazione diverse:
    \begin{description}
        \item[pause:] si aggrega con le parentesi
        \item[parentesi:] si aggrega con le pause
        \item[misto:] si aggrega con le pause nel livello più interno e con le parentesi in quelli più esterni
    \end{description}
    \begin{block}{Esempio}
        \begin{description}
            \item[pause:] \textit{a meno} <P><P> \textit{b diviso c} <P> \textit{per d}  <P> 
            \item[parentesi:] \textit{a meno} ( ( \textit{b diviso c} ) \textit{per d}  ) 
            \item[misto:] \textit{a meno} ( <P> \textit{b diviso c} <P>\textit{per d})
        \end{description}
    \end{block}
\end{frame}
\section{Architettura del sistema software}
\begin{frame}{Architettura del sistema software}
    \centering
    \begin{tikzpicture}
        \tikzstyle{state}=[shape=rectangle, rounded corners,
        draw, align=center, minimum size=1cm, minimum width=2.5cm,
        top color=white, bottom color=blue!20]
        
        \node [state] (postprocessor) at (-3,3) {PostProcessor};
        \node [state] (latexml) at (-3,0) {LatexML};
        \node         (start) [left of=latexml,node distance=3cm, coordinate] {b};
        \node [state] (fts) at (3,3) {F-T-S};
        \node [state] (x) at (3,0) {SynthCaller};
        \node (end) [right of=x,node distance=3cm, coordinate] {b};

        \path[->] (start) edge node [above=10]{\LaTeX{}} (latexml);
        \path[->,>=stealth]
        (latexml)   edge node[above=5, rotate=90] {cmml}  (postprocessor) 
        (postprocessor) edge  node[above=5] {cmml trattato}  (fts)
        (fts) edge node[above=5,  rotate=270] {testo}  (x)
        (x) edge node[above=5] {audio}  (end);
    \end{tikzpicture}
\end{frame}

\section{Sperimentazione}
\begin{frame}{Formule semplici}
    \centering
    \renewcommand{\arraystretch}{2}
    \begin{tabular}{ |c|c| } 
        \hline
        \textbf{Formula \LaTeX} &  \textbf{Nodi} \\
        \hline
        $\displaystyle{A\times B=\{(x,y) \mid x\in A,y\in B\}}$ & 15\\
        \hline
        % $\displaystyle{\sum_{k=1}^{n}k=\frac{n(n+1)}{2}}$ &16 &0\\
        $\displaystyle{g^{-1}(y)=f^{-1}\left((y-b)/a\right)}$ &13 \\
        \hline
        $\displaystyle{\int_{b}^{c}a\;\mbox{d}x=a(c-b)}$ &14\\
        \hline
        $\displaystyle{x>b\Longrightarrow|f(x)|<M}$ &10 \\
        \hline
        $\displaystyle{\sqrt[n]{x}=x^{1/n}}$ &10 \\
        \hline
    \end{tabular}
    \renewcommand{\arraystretch}{1}
\end{frame}

\begin{frame}{Formule complesse}
    \centering
    \renewcommand{\arraystretch}{2}
    \begin{tabular}{ |c|c| } 
        \hline
        \textbf{Formula \LaTeX} &  \textbf{Nodi}\\
        \hline
        $\displaystyle{\lim_{x\to x_{0}}\left\{\frac{f(x)-f(x_{0})}{x-x_{0}}-f^{\prime}(x_{0})\right\}=0 }$ & 31\\
        \hline
        $\displaystyle{y=f(a)+\frac{f(b)-f(a)}{b-a}(x-a)}$ &21 \\
        \hline
        $\displaystyle{\int\frac{1}{\sqrt{m^{2}-x^{2}}}\mbox{d}x=\arcsin\frac{x}{m}+c}$ &20 \\
        \hline
        $\displaystyle{\sum_{k=0}^{n}\frac{f^{(k)}(x_{0})}{k!}(x-x_{0})^{k}}$ &28 \\
        \hline
        $\displaystyle{\lim\left(1+\frac{1}{n}\right)^{n}=e }$ &10 \\
        \hline
    \end{tabular}
\end{frame}
\begin{frame}{Esempio di sintesi}
    \begin{block}{}
        \begin{enumerate}
            \item \href{https://www.youtube.com/watch?v=vIP27Ul7aiQ}{Formula semplice, smart, ibm}
            $$\sqrt[n]{x}=x^{1/n}$$
            \item \href{https://www.youtube.com/watch?v=mvRExNw4iNs}{Formula semplice, smart, espeak}:
            $$C\times D=\{(z,y) \mid z\in C,y\in D\}$$
            \item \href{https://www.youtube.com/watch?v=WGwa3dlEwBg}{Formula complessa, pause, ibm}
            $$\sum_{k=0}^{n}\frac{g^{(k)}(x_{0})}{k!}(x-x_{0})^{k} $$
            \item \href{https://www.youtube.com/watch?v=b0wQdg0babY}{Formula complessa, smart,espeak}:
            $$\lim\left(1+\frac{1}{t}\right)^{t}=e$$
        \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}{Risultati formule semplici}
    \begin{block}{formule semplici}
        \centering
        \begin{tabularx}{\textwidth}{ c c Y Y } 
        \hline
        \textbf{Sintetizzatore} &  \textbf{Aggregazione} & \textbf{\% Totale Corrette} & \textbf{\% Corrette senza errore S } \\
        \hline
        ibm & smart & 65 & 70\\
        espeak & smart & 100 & 100\\
        \hline
        \end{tabularx}    
    \end{block}
    \begin{block}{Formule complesse}
        \begin{tabularx}{\textwidth}{ c c Y Y } 
            \hline
            \textbf{Sintetizzatore} &  \textbf{Aggregazione}& \textbf{\% Totale Corrette} & \textbf{\% Corrette senza errore S } \\
            \hline
            ibm & pause & 60 & 66.6\\
            ibm & par & 55 & 60\\
            ibm & smart & 50 & 62.5\\
            espeak & - & 50 & 50\\
            \hline
        \end{tabularx}
    \end{block}
\end{frame}
\section{Conclusioni}
\begin{frame}{Il lavoro svolto}
    \begin{enumerate}
        \item Si è indagato sulla natura linguistica delle formule e si è stabilita la struttura linguistica associata a ogni categoria di operatori matematici.
        \item Si è studiata la differenza tra notazione matematica e frasi matematiche. Ci si è concentrati sull'aggregazione delle formule e si sono presentate tre diverse strategie.
        \item Si è sviluppato un software in clojure in grado di sintetizzare le formule matematiche.
        \item Si è svolta una sperimentazione con soggetti con disabilità visive a cui si è chiesto di testare il sistema e dare una sua valutazione.    
    \end{enumerate}
\end{frame}

\begin{frame}{Possibili lavori futuri}
    \begin{itemize}
        \item Estendere l'insieme degli operatori gestiti dal sistema \item Creare altri dizionari matematici per poter gestire la sintesi anche in altre lingue
        \item Sviluppare un sistema che permetta di ottenere il \textit{Content MathML} in modo più semplice
        \item Sviluppare un software che permetta la navigazione della formula
        \item Integrare il sistema con altri strumenti, in particolar modo i browser
        \item Usare nella valutazione dei risultati la tecnica adoptata normalmente nell’ambito del parsing del linguaggio naturale, come ad esempio \textit{Eval B} o \textit{Parseval}
    \end{itemize}
\end{frame}
\begin{frame}{}
    \begin{block}{}
        \begin{center}
            Grazie per l'attenzione
        \end{center}
    \end{block}
\end{frame}
\end{document}
